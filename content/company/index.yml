---
  title: About GitLab
  description: Learn more about GitLab and what makes us tick.
  components:
    - name: 'solutions-hero'
      data:
        title: About GitLab
        subtitle: Behind the scenes of The DevSecOps Platform
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        image:
          image_url: /nuxt-images/company/company_hero.png
          image_url_mobile: /nuxt-images/company/company_hero.png
          alt: "GitLab team members"
          bordered: true
          rectangular: true
    - name: 'copy-about'
      data:
        title: "What we do"
        subtitle: "We're the company behind GitLab, the most comprehensive DevSecOps platform."
        description: |
          What started in 2011 as an open source project to help one team of programmers collaborate is now the platform millions of people use to deliver software faster, more efficiently, while strengthening security and compliance.
          \
          Since the beginning, we've been firm believers in remote work, open source, DevSecOps, and iteration. We get up and log on in the morning (or whenever we choose to start our days) to work alongside the GitLab community to deliver new innovations every month that help teams focus on shipping great code faster, not their toolchain.
        cta_text: 'Learn more about GitLab'
        cta_link: '/why-gitlab/'
        data_ga_name: "about gitlab"
        data_ga_location: "body"
    - name: 'copy-numbers'
      data:
        title: GitLab by the numbers
        rows:
          - item:
            - col: 4
              title: |
                **Code**
                **contributors**
              number: "3,300+"
              link: http://contributors.gitlab.com/
              data_ga_name: contributors
              data_ga_location: body
            - col: 4
              title: |
                **Offices**
                All-remote since inception
              number: "0"
          - item:
            - col: 4
              title: |
                **Success rate**
                of shipping monthly releases of GitLab, the product
              number: "100%"
              link: /releases/
              data_ga_name: releases
              data_ga_location: body
            - col: 4
              title: |
                **Team members**
                in 60+ countries
              number: "1,800+"
              link: /company/team/
              data_ga_name: team
              data_ga_location: body
          - item:
            - col: 8
              title: '**Estimated registered users**'
              number: "Over 30 million"
        customer_logos_block:
          showcased_enterprises:
            - image_url: "/nuxt-images/home/logo_tmobile_white.svg"
              link_label: Link to T-Mobile and GitLab webcast landing page"
              alt: "T-Mobile logo"
              url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
            - image_url: "/nuxt-images/home/logo_goldman_sachs_white.svg"
              link_label: Link to Goldman Sachs customer case study
              alt: "Goldman Sachs logo"
              url: "/customers/goldman-sachs/"
            - image_url: "/nuxt-images/home/logo_cncf_white.svg"
              link_label: Link to Cloud Native Computing Foundation customer case study
              alt: "Cloud Native logo"
              url: /customers/cncf/
            - image_url: "/nuxt-images/home/logo_siemens_white.svg"
              link_label: Link to Siemens customer case study
              alt: "Siemens logo"
              url: /customers/siemens/
            - image_url: "/nuxt-images/home/logo_nvidia_white.svg"
              link_label: Link to Nvidia customer case study
              alt: "Nvidia logo"
              url: /customers/nvidia/
            - image_url: "/nuxt-images/home/logo_ubs_white.svg"
              link_label: Link to blogpost How UBS created their own DevOps platform using GitLab
              alt: "UBS logo"
              url: /blog/2021/08/04/ubs-gitlab-devops-platform/
        cta_title: |
          From planning to production,
          bring teams together in one application
        cta_text: "Get free trial"
        cta_link: "https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
        data_ga_name: "free trial"
        data_ga_location: "body"
    - name: 'copy-mission'
      data:
        title: "GitLab's mission"
        description: "Our mission is to make it so that **everyone can contribute**. When everyone can contribute, users become contributors and we greatly increase the rate of innovation."
        cta_text: 'Learn more'
        cta_link: '/company/mission/'
        data_ga_name: "mission"
        data_ga_location: "body"
    - name: 'showcase'
      data:
        title: GitLab's values
        items:
          - title: Collaboration
            icon:
              name: collaboration-alt-4
              alt: Collaboration Icon
              variant: marketing
            text: "We prioritize things that help us work together effectively, such as assuming positive intent, saying “thanks” and “sorry,” and giving timely feedback."
            link:
              href: /handbook/values/#collaboration
              data_ga_name: Collaboration
              data_ga_location: body
          - title: Results
            icon:
              name: increase
              alt: increase Icon
              variant: marketing
            text: "We operate with a sense of urgency and bias for action, and we do what we promise — to each other, customers, users, and investors."
            link:
              href: /handbook/values/#results
              data_ga_name: results
              data_ga_location: body
          - title: Efficiency
            icon:
              name: digital-transformation
              alt: efficiency Icon
              variant: marketing
            text: "From choosing boring solutions to documenting everything and being managers of one, we strive to make fast progress on the right things."
            link:
              href: /handbook/values/#efficiency
              data_ga_name: efficiency
              data_ga_location: body
          - title: "Diversity, Inclusion, & Belonging"
            icon:
              name: community
              alt: community Icon
              variant: marketing
            text: "We work to ensure GitLab is a place where people from every background and circumstance feel like they belong and can thrive."
            link:
              href: "/handbook/values/#diversity-inclusion"
              data_ga_name: diversity inclusion
              data_ga_location: body
          - title: Iteration
            icon:
              name: continuous-delivery
              alt: continuous delivery Icon
              variant: marketing
            text: "We aim to do the smallest viable and valuable thing, and get it out quickly for feedback."
            link:
              href: "/handbook/values/#iteration"
              data_ga_name: iteration
              data_ga_location: body
          - title: Transparency
            icon:
              name: open-book
              alt: book Icon
              variant: marketing
            text: "Everything we do is public by default, from our company handbook to the issue trackers for our product."
            link:
              href: "/handbook/values/#transparency"
              data_ga_name: transparency
              data_ga_location: body
    - name: 'timeline'
      data:
        title: "GitLab over the years"
        items:
          - year: "2011"
            description: "The GitLab project began with a commit"
          - year: ""
            description: "We started releasing a new version of GitLab on the the 22nd of every month"
          - year: "2012"
            description: "The first version of GitLab CI is created"
          - year: "2014"
            description: "GitLab is incorporated"
          - year: "2015"
            description: "Joined Y Combinator and published the GitLab Handbook to our website repository"
          - year: "2016"
            description: "Announced our master plan and raised $20 million in B round financing"
          - year: "2021"
            description: "GitLab Inc. became a publicly traded company on the Nasdaq Global Market (NASDAQ: GTLB)"
    - name: 'copy-about'
      data:
        title: "Working at GitLab"
        description: |
          We strive to create an all-remote environment where all team members around the world can show up as their full selves, contribute their best, feel their voices are heard and welcomed, and truly prioritize work-life balance.
          \
          If you're interested in being a part of the team, we invite you to [learn more about working at GitLab](/jobs/) and apply to any open positions that look like a good fit.
        cta_text: 'View all open roles'
        cta_link: '/jobs/all-jobs/'
        data_ga_name: "all jobs"
        data_ga_location: "body"
    - name: 'teamops'
      data:
        image: "/nuxt-images/company/company-teamops.svg"
        image_alt: "coworkers sharing documents"
        mobile_img: "/nuxt-images/company/TeamOps-mobile.svg"
        header_img: "/nuxt-images/company/TeamOps.svg"
        title: "Better teams. Faster progress. Better world."
        description: "TeamOps is GitLab's unique people practice which makes teamwork an objective discipline. It's how GitLab scaled from a startup to a global public company in a decade. Through a free and accessible practitioner certification, other organizations can leverage TeamOps to make better decisions, deliver results, and move our world forward."
        button:
            link: "/teamops/"
            text: "Learn more about TeamOps"
            data_ga_name: "learn more about teamops"
            data_ga_location: "body"
        link:
            link: "https://levelup.gitlab.com/learn/course/teamops"
            text: "Get certified"
            data_ga_name: "get certified"
            data_ga_location: "body"
    - name: 'learn-more-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
              hex_color: "#171321"
            event_type: "Blog"
            header: "See what's new with GitLab, DevOps, security, and more."
            link_text: "Visit the blog"
            href: "/blog/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLab logo
            data_ga_name: "blog"
            data_ga_location: "body"
          - icon:
              name: announcement
              variant: marketing
              alt: Announcement Icon
              hex_color: "#171321"
            event_type: "Press room"
            header: "Recent news, press releases, and our press kit."
            link_text: "Learn more"
            href: "/press/press-kit/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLab logo
            data_ga_name: "press kit"
            data_ga_location: "body"
          - icon:
              name: money
              variant: marketing
              alt: Money Icon
              hex_color: "#171321"
            event_type: "Investor relations"
            header: "The latest stock and financial information for investors."
            link_text: "Learn more"
            href: "https://ir.gitlab.com/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLab logo
            data_ga_name: "investor relations"
            data_ga_location: "body"

