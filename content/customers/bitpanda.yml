---
  data:
    title: Bitpanda
    description: GitLab enables robust scaling for hypergrowth fintech Bitpanda
    og_title: Bitpanda
    twitter_description: GitLab enables robust scaling for hypergrowth fintech Bitpanda
    og_description: GitLab enables robust scaling for hypergrowth fintech Bitpanda
    og_image: /nuxt-images/blogimages/bitpanda_cover_image.jpg
    twitter_image: /nuxt-images/blogimages/bitpanda_cover_image.jpg

    customer: Bitpanda
    customer_logo: /nuxt-images/logos/U-bitpanda-logo.svg
    heading: Bitpanda uses GitLab to enable dynamic growth of its DevOps function
    key_benefits:
      - label: Fast developer onboarding
        icon: dev-enablement
      - label: Deep visibility into project status
        icon: visibility
      - label: Scalable support
        icon: auto-scale
    header_image: /nuxt-images/blogimages/bitpanda_cover_image.jpg
    customer_industry: Financial Services
    customer_employee_count: 600+
    customer_location: Vienna, Austria
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    blurb: Digital assets, cryptocurrencies, blockchain technology, and commission-free fractional stocks are just some of the expansive services the Bitpanda technical team builds and deploys.
    introduction: |
      GitLab enables robust scaling for hypergrowth fintech Bitpanda
    quotes:
      - text: |
          We don’t have a fixed release day. We are deploying every day, every hour.
        author: David Papp
        author_role: Head of development
        author_company: Bitpanda
    content:
      - title: Empowering individual investors through innovative fintech
        description: |
          [Bitpanda](https://www.bitpanda.com/en), based in Vienna, Austria, is an innovative 7-year-old financial technology (fintech) company. Rooted on the tenet that investing should be safe, easy and accessible for everyone, the company has built a digital investment service, a ​​user-friendly, trade-everything platform created to empower users to invest in stocks, cryptocurrencies and metals – with any amount of money. Working to redefine investment possibilities for people who might be left on the margins of traditional finance, Bitpanda has grown to have more than 3 million users. Focused on innovative technology, like digital assets and blockchain, that aims to compete with traditional financial methods, Bitpanda’s solutions allow both established and emerging players to accelerate digital transformation and customize products for end users. Bitpanda’s DevOps teams collaborate to create and deploy innovative, cutting-edge systems that support the company’s mission to empower individual investors.
      - title: Effectively support scalable, dynamic growth of teams and pipelines
        description: |
          Bitpanda has grown dramatically, going from just two or three developers relying on Subversion to creating teams with several hundred developers in less than a decade. To support Bitpanda’s mission and to keep up with this growth, its DevOps teams needed to gain better visibility into their fast-paced and complicated work. As the number of pipelines multiplied, tracking development and deployment processes became more complicated. At the same time, code version and control point tools, like Subversion, were unable to meet these necessary objectives. It wasn’t tenable. To fix the situation, they began searching for a unifying approach, evaluating platforms from GitLab, GitBucket, GitHub and Cheetah. The answer lay with GitLab. And it’s been the answer for every team at Bitpanda.
      - title: Efficient operations and simplified workflow
        description: |
          GitLab provides an integrated solution for pipeline creation that enables developers to be self-sufficient. With GitLab CI/CD, live updates are easier to accomplish, supporting continuous release of services. Through use of multiple GitLab labels, epics and boards, each team can readily monitor work underway. This supports simplified workflow and greater operational efficiency, while allowing scrum leads and others to tailor tooling around individual requirements. And even rollbacks, when necessary, are supported, as well. To simplify work even more, GitLab tools are a ready fit with the AWS Elastic Kubernetes Service. Since Bitpanda has been dramatically growing its team of developers, another major benefit of going with GitLab has been efficiently onboarding new engineers. Being able to rapidly and successfully scale up their DevOps teams has enabled the company to continue to be innovative and address new markets.
      - title: One DevOps platform enables agile productivity and maintains high standards
        description: |
          By adopting GitLab Premium, Bitpanda has been able to build and deploy pipelines using a single, comprehensive tool. “In the end, we decided on GitLab because it was a tool that basically had everything in one place,” said Christian Trummer, chief technology officer at Bitpanda. The software allows teams to quickly move work forward, while ensuring quality and compliance, and adapting to spikes in workloads. GitLab removes the need for its DevOps teams to spend extensive time and money doing heavy, manual work to obtain operation metrics. The tools uncover link issues across teams, while also monitoring lead times, cycle times, sprint editions and plan-versus-actual status. In regulated industries, this kind of monitoring is crucial to compliance. “With GitLab we know we have a well defined change process. GitLab is very intuitive, when you compare it with Jira, which is much more complicated. It is very handy in terms of audits. There is always full visibility,” said Trummer, who also commended GitLab for transparency in pricing. “With GitLab, everything is open.”

