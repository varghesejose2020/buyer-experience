---
  data:
    title: European Space Agency
    description: Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions
    og_title: European Space Agency
    twitter_description: Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions
    og_description: Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions
    og_image: /nuxt-images/blogimages/ESA_case_study_image.jpg
    twitter_image: /nuxt-images/blogimages/ESA_case_study_image.jpg

    customer: European Space Agency
    customer_logo: /nuxt-images/logos/esa-logo.svg
    heading: How the European Space Agency uses GitLab to focus on space missions
    key_benefits:
      - label: Simplified toolchain
        icon: cog
      - label: Faster code deploys
        icon: code
      - label: Improved collaboration
        icon: collaboration
    header_image: /nuxt-images/blogimages/ESA_case_study_image.jpg
    customer_industry: Science and Research
    customer_employee_count: 5,000
    customer_location: France, Netherlands, Italy, Germany, Spain, United Kingdom, Belgium
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: of ESA employees using GitLab
        stat: ~15%
      - label: groups
        stat: 140
      - label: jobs the first year
        stat: 60k+
    blurb: The European Space Agency (ESA) has always carefully deployed new technologies. But without a central version control system in place, opportunities for collaboration, synergies and multiple exploitations of effort were less visible.
    introduction: |
     With GitLab, teams across ESA can now collaborate and share code and insights both within their teams and with other teams. GitLab is allowing teams to cross borders, increase cooperation and reshape working culture.
    quotes:
      - text: |
          To see people collaborating is contagious. People are saying, ‘Hey, I want to have a project like that!’ or ‘I want to participate.’ These kinds of things can happen because everybody has access,
        author: Redouane Boumghar
        author_role: Research Fellow
        author_company: ESA
      - text: |
          I think our use case is quite interesting at ESA. Our team develops software, and we are also flying spacecraft. So developing software is in addition to our main activity. There are a lot of tools that we
          require to support us in our actual job. In that sense, we want to minimize the time that we spend to build these tools. Furthermore,
          GitLab has also shown that it is very effective in sharing our tools with other teams.
        author: Bruno Sousa
        author_role: Spacecraft Operations Manager
        author_company: ESA
    content:
      - title: Leading the charge as Europe’s gateway to space
        description: |
          The European Space Agency (ESA) provides a European-wide Space Programme. ESA's programmes are designed to find out more about Earth, its immediate space environment, our Solar System, and the Universe, as well as to develop satellite-based technologies and services and to promote European industries. ESA also works closely with space organizations outside Europe.
          
          ESA is responsible for coordinating the financial and intellectual resources of its 22 member states to ensure that investment in space continues to deliver benefits to the citizens of Europe and the world. The organization focuses on a wide variety of missions focused on space exploration and continued research. Some of their recent missions include sending orbiters to Mercury and studying hypervelocity stars in the Milky Way.

      - title: Lacking version control and collaboration
        description:  |
          TESAhas always carefully deployed new technologies. Validation and security have been at the fore, and there is less drive to invoke newer practices or technologies for their own sake unless they clearly bring added benefits for ESA’s core business. This sometimes resulted in using older, trusted tools to share code, at the expense of timeliness.

          In 2015 different teams within ESA were using a heterogeneous approach to control systems, such as Subversion or CVS. The emergence of Git, and its subsequent adoption by the ESA IT Department, was a harmonious intersection of user needs and secure technology. GitLab was validated and adopted by ESA as a code repository platform in 2016. Usage was initially limited to a hand-picked group of first-wave users, but demand quickly escalated.
          
          In just two years, more than 140 groups adopted GitLab as their software versioning tool. Across ESA, more than 1,500 software projects have been created. These range from mission control systems and onboard software for spacecraft to image processing and monitoring tools for labs. The ESA IT Department also uses GitLab to host their code tools and configurations infrastructure.

      - title: Creating a culture where collaboration is contagious
        description: |
          GitLab was introduced to the ESA population in 2016. Teams across Europe embraced the tool at all ESA establishments and sites. They can now collaborate and share code and insights both within their teams and with other teams. The process is faster, in real time, and produces reliable, stable results. Users can use more of their time to focus on their mission-critical tasks and spend less time keeping tools running.
          
          The adoption rate was high. Within one week 40 projects were running in GitLab. “Right now, we have 15% of our user population using GitLab”, a representative from the ESA IT Department GitLab project commented.
          
          For ESA, this represents a departure from the previous software development culture. In the past, it was assumed that there were fewer synergies to be exploited. Version control systems were individualised, or teams had not implemented them. As the technology now matches ESA’s needs, GitLab lets ESA approach more standardisation and efficiency.

          Initially, ESA implemented GitLab to exploit the version control capabilities within the tool. However, the user community has also benefitted from the continuous integration and delivery (CI/CD) capabilities within GitLab. ESA began using CI/CD capabilities in November 2017, and there are currently 60,000 jobs in GitLab. Feedback to the ESA IT Department from the user community has conveyed the user’s satisfaction with this development. One user reported, “We initially started using version control, but we discovered that we could use CI on our project. We tried it out and were immensely impressed with how well it all worked together”.

          ESA’s diversity of teams and tasks provides unique challenges. Bruno Sousa explains why his tasks require CI/CD, saying, “For our use case, in particular, the CI/CD capabilities are extremely important. In my role I am simultaneously responsible for flying a spacecraft and developing a tool for us, and also potentially for other missions. I don’t have the time to deploy the software over and over again, so GitLab is very helpful in facilitating the whole process. It makes everything easier so that I can focus on my core task of flying the spacecraft.”

      - title: Increasing excitement and speed around code deployments
        description: |
          GitLab has provided a software development turnaround speed that ESA had not previously been able to achieve. The code is now continuously deployed in a matter of minutes, when previously it may have taken weeks.

          GitLab is able to address challenges along many stages of the software development pipeline. In the past, different ESA teams were using a variety of CI/CD engines. Now they are being replaced with GitLab CI because GitLab is more user-friendly. As more users move to GitLab, ESA’s obligation to maintain other tools is removed. GitLab CI is then integrated into more version control systems.

          The automation in GitLab also saves ESA IT Department resources. With operation and backup fully automated, IT Specialists can focus on monitoring the tool and, importantly, addressing more IT challenges for the agency.
