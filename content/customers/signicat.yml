---
  data:
    title: Signicat
    description: Signicat reduces deploy time from days to just minutes
    og_title: Signicat
    twitter_description: Signicat reduces deploy time from days to just minutes
    og_description: Signicat reduces deploy time from days to just minutes
    og_image: /nuxt-images/blogimages/signicat_cover_image.jpg
    twitter_image: /nuxt-images/blogimages/signicat_cover_image.jpg

    customer: Signicat
    customer_logo: /nuxt-images/logos/signicat_logo.png
    heading: Signicat reduces deploy time from days to just minutes
    key_benefits:
      - label: Rapid pipeline deployment
        icon: gitlab-pipeline-alt
      - label: Improved collaboration
        icon: collaboration-alt-4
      - label: Easy scalability
        icon: auto-scale
    header_image: /nuxt-images/blogimages/signicat_cover_image.jpg
    customer_industry: Technology
    customer_employee_count: 400+
    customer_location: Trondheim, Norway
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - stat: 500
        label: daily builds
      - stat: 2x
        label: increase in developer team
    blurb: Quick development and deployment of reliable identity services was a must-have at Signicat. 
    introduction: |
      The company turned to GitLab Premium SaaS to power onboarding and drive collaboration.
    quotes:
      - text: |
          The ability to react to change quickly is extremely valuable for a scaling company like ourselves. We are seeing a lot of collaboration around GitLab CI/CD and pipelines.
        author: Jon Skarpeteig
        author_role: Vice President for Cloud Architecture
        author_company: Signicat
    content:
      - title: A market-leading European digital identity solutions provide
        description: |
          [Signicat](https://www.signicat.com/) is a market-leading European digital identity solutions provider that has aggressively extended its capabilities, both organically and through acquisitions, to create a full line of identity management tooling, including identity proofing solutions, a suite of electronic signature solutions, and advanced authentication. Clients, partners and customers include Fortune 500 banks and financial services, government and health services, payments, insurance and other industries. While supporting more than 30 national identity methods, Signicat enables digital transformation in regulated and unregulated industries.
          
          The company’s software services focus on essential use cases, including trustworthy digital onboarding and reboarding, identity verification and authentication, risk and compliance management, and fraud detection. Capabilities have been expanded, just as the threat surfaces of modern B2B and B2C applications have expanded. This is particularly crucial as clients focus their efforts on better knowing, identifying, and protecting their customers, especially during the COVID-19 pandemic. Advanced use of cloud-native software development methods underlies Signicat’s efforts to meet the evolving needs for ever-better digital identity technologies and building a trusted digital world.
      - title: Achieving rapid, secure deployment while improving service deliveries
        description: |
          The challenge Signicat faced were manual processes which needed to be automated, and fragmentation across tooling in different teams. Signicat also needed a solution to continually expand as the business grew, which called for an exceptionally cohesive integration architecture — one that must adhere to the strictest standards of governance and compliance. The need to continue promoting collaboration across diverse working teams was also important. It was important to know the status of work, and which person or team could help on specific code dependencies.

          Development leaders looked for an overarching platform that allowed them to move quickly, and to, in effect, accomplish more with less. What they needed was a DevOps platform that future-proofed them and aligned with their ambitious roadmap. To get there, they went with GitLab to achieve rapid, dependable, development cycles and scalable performance through various peaks of operations.
      - title: Scalability, visibility, and enhanced integration
        description: |
          GitLab Premium SaaS, ideally suited for scaling organizations and multi-team use, is used to improve the developer experience, while enhancing productivity. Developers are onboarded quickly, enabling them to get important workloads up and running. Scalable, reliable, and predictable build timetables also help fine-tune operations. GitLab enables integration with a variety of static application security testing and other popular third-party tools, which is especially crucial to a company that routinely adds to its technology portfolio via an aggressive acquisition strategy. The platform provides visibility into project status, and supports cross-team communications. This has given developer teams more empowerment and autonomy, which had been another key objective for Signicat. Improvements in visibility also took the form of fine-grained code documentation, used for audits as part of necessary certifications and verifications. The platform also meshed well with the company’s move to containerization of software services.
      - title: Huge gains in deployment times and culture alignment
        description: |
          GitLab provides developer teams a platform for reliable, scalable and predictable build times. Collaboration, resourcing and planning have improved as well. “A positive thing we are seeing is in terms of GitLab support. Instead of having to set aside resources internally, we can direct our internal users to GitLab support” added Jon. One of the most noticeable and dramatic improvements since adopting GitLab is that Signicat’s deployment time has dropped from days to just minutes. It has significantly changed not only their speed to deployment but it has supported Signicat with the ability to be agile and deliver more continuously. GitLab’s dedication to transparency and openness in its technology roadmap is a notable plus for planning, according to Jon Skarpeteig, Signicat’s Vice-President for Cloud Architecture. “The push from the company perspective is towards containerizing everything. That is one of the reasons why GitLab was a good fit for us. It really solves containers in a very elegant way” he explains. That transparent GitLab roadmap matches Signicat’s own roadmap, providing an integrated system for source code management, continuous integration and continuous delivery of key services – including Docker and Kubernetes.

          “GitLab’s open culture has served us well. There is an openness about what is coming and how things get prioritized,” said Skarpeteig, adding that GitLab support is another benefit. GitLab’s support resources for Signicat’s internal developers’ free up development managers’ time. In addition, GitLab’s software supports the team’s ability to quickly react to change; which is extremely valuable for a company in rapid growth at the same time it is at the center of important trends in modern commerce and technology.

