---
  title: Starten Sie durch für kleine Unternehmen
  description: 'Sie müssen schnell auf den Markt kommen und Innovationen schneller als die Konkurrenz entwickeln. Dieser Leitfaden zeigt Ihnen den Einstieg in GitLab, die perfekte Lösung für kleine Unternehmen'
  side_menu:
    anchors:
      text: Auf dieser Seite
      data:
      - text: Einstieg
        href: "#Einstieg"
        data_ga_name: getting-started
        data_ga_location: side-navigation
      - text: Einrichtung durchführen
        href: "#getting-setup"
        data_ga_name: getting-setup
        data_ga_location: side-navigation
      - text: Mit GitLab
        href: "#using-gitlab"
        data_ga_name: using-gitlab
        data_ga_location: side-navigation
  hero:
    crumbs:
      - title: Loslegen
        href: /loslegen/
        data_ga_name: Get Started
        data_ga_location: breadcrumb
      - title: Starten Sie durch für kleine Unternehmen
    header_label: 20 Minuten bis zur Fertigstellung
    title: Starten Sie durch für kleine Unternehmen
    content: |
      "Sie müssen schnell auf den Markt kommen und Innovationen schneller als die Konkurrenz entwickeln. Sie können es sich nicht leisten, durch einen komplizierten DevSecOps-Prozess ausgebremst zu werden. Dieser Leitfaden hilft Ihnen dabei, schnell die Grundlagen für die automatisierte Softwareentwicklung und -bereitstellung auf der Premium-Stufe einzurichten, mit Optionen wie Sicherheit, Compliance und Projektplanung, die in der Ultimate-Stufe zu finden sind.\n"
  copy_info:
    title: Bevor du anfängst
    text: |
      In GitLab 15.1 (22. Juni 2022) und höher sind Namespaces in GitLab.com im Free-Tarif auf fünf (5) Mitglieder pro <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">Namespace</a> beschränkt. Dieses Limit gilt für Gruppen der obersten Ebene und persönliche Namespaces. Wenn Sie mehr Benutzer haben, empfehlen wir, mit einer kostenpflichtigen Stufe zu beginnen.
  steps:
    groups:
      - header: Einstieg
        show: Zeige alles
        hide: Alles verstecken
        id: Einstieg
        items:
          - title: Finden Sie heraus, welches Abonnement das richtige für Sie ist
            copies:
              - title: GitLab SaaS oder GitLab selbstverwaltet
                text: |
                  <p>Möchten Sie, dass GitLab Ihre GitLab-Plattform verwaltet, oder möchten Sie sie selbst verwalten? </p>
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed
                  text: Bewerten Sie die Unterschiede
                  ga_name: GitLab Saas vs Self-Managed
                  ga_location: body
          - title: Bestimmen Sie, welche Stufe Ihren Anforderungen entspricht
            copies:
              - title: Kostenlos, Premium oder Ultimate
                text: |
                  <p>Um herauszufinden, welche Stufe für Sie die richtige ist, berücksichtigen Sie Folgendes: </p>

              - title: Anzahl der Nutzer
                text: |
                  GitLab-Abonnements verwenden ein gleichzeitiges (Sitz-)Modell sowohl für SaaS als auch für die Selbstverwaltung. Die Anzahl der Benutzer/Plätze kann Ihre Wahl der Stufe beeinflussen. Wenn Sie mehr als fünf Benutzer haben, ist eine kostenpflichtige Stufe (Premium oder Ultimate) erforderlich.
              - title: Benötigter Speicherplatz
                text: |
                  <p>Für kostenlose Namespaces auf GitLab SaaS gilt ein Speicherlimit von 5 GB.</p>
              - title: Gewünschte Sicherheit und Compliance
                text: |
                  <p>* Secrets-Erkennung, SAST und Container-Scanning sind in Free und Premium verfügbar.</p>
                  <p>* Zusätzliche Scanner <a href="https://docs.gitlab.com/ee/user/application_security/">wie DAST</a>, Abhängigkeiten, Cluster-Images, IaC, APIs und Fuzzing sind in Ultimate verfügbar.</p>
                  <p>* Umsetzbare Ergebnisse, die in die Merge-Request-Pipeline und das Sicherheits-Dashboard integriert werden, erfordern Ultimate für das Schwachstellenmanagement.</p>
                  <p>* Compliance-Pipelines erfordern Ultimate.</p>
                  <p>* Lesen Sie mehr über <a href="https://docs.gitlab.com/ee/user/application_security/">unsere Sicherheitsscanner </a> und unsere <a href="https://docs.gitlab.com/ee/administration/compliance.html">Compliance-Fähigkeiten</a>.
                link:
                  href: /pricing/
                  text: Besuchen Sie unsere Preise, um mehr zu erfahren
                  ga_name: pricing
                  ga_location: body
      - header: Einrichtung durchführen
        show: Zeige alles
        hide: Alles verstecken
        items:
          - title: Richten Sie Ihr SaaS-Abonnementkonto ein
            copies:
              - title: Bestimmen Sie, wie viele Sitzplätze Sie wünschen
                text: |
                  Ein GitLab SaaS-Abonnement verwendet ein gleichzeitiges (Sitz-)Modell. Sie bezahlen ein Abonnement entsprechend der maximalen Benutzeranzahl im Abrechnungszeitraum. Sie können während des Abonnementzeitraums Benutzer hinzufügen und entfernen, solange die Gesamtzahl der Benutzer zu einem bestimmten Zeitpunkt die Anzahl der Abonnements nicht überschreitet.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined
                  text: Erfahren Sie, wie die Sitzplatznutzung ermittelt wird
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: Besorgen Sie sich Ihr SaaS-Abonnement
                text: |
                  <p>GitLab SaaS ist das Software-as-a-Service-Angebot von GitLab, das auf GitLab.com verfügbar ist. Sie müssen nichts installieren, um GitLab SaaS zu nutzen, Sie müssen sich lediglich anmelden. Das Abonnement bestimmt, welche Funktionen für Ihre privaten Projekte verfügbar sind. Gehen Sie zur Preisseite und wählen Sie **Premium kaufen** oder **Ultimate kaufen**.  <p/>
                  <p>Organisationen mit öffentlichen Open-Source-Projekten können sich aktiv für unser GitLab for Open Source-Programm bewerben. Funktionen von <a href="/pricing/ultimate">GitLab Ultimate</a>, einschließlich 50.000 Minuten berechnen, stehen qualifizierten Open-Source-Projekten über das <a href="/solutions/open-source">GitLab for Open Source-</a> Programm kostenlos zur Verfügung.<p/>
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#obtain-a-gitlab-saas-subscription
                  text: Erfahren Sie mehr über ein SaaS-Abonnement
                  ga_name: Obtain your SaaS subscription
                  ga_location: body
              - title: Bestimmen Sie die benötigten CI/CD-Shared-Runner-Minuten
                text: |
                  <a href="https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners" data-ga-name="Shared Runners" data-ga-location="body">Freigegebene Läufer</a> werden mit jedem Projekt und jeder Gruppe in einer GitLab-Instanz geteilt. Wenn Jobs auf gemeinsam genutzten Läufern ausgeführt werden, werden Minuten berechnen verwendet. Auf GitLab.com wird das Kontingent an Minuten berechnen für jeden <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">Namespace</a> festgelegt und durch <a href="/pricing" data-ga-name="Your license tier" data-ga-location="body">Ihre Lizenzstufe bestimmt</a>.<p/><p>Zusätzlich zum monatlichen Kontingent können Sie auf GitLab.com bei Bedarf <a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes" data-ga-name="purchase additional compute minutes" data-ga-location="body">zusätzliche Minuten berechnen erwerben</a>.</p>

          - title: Richten Sie Ihr selbstverwaltetes Abonnementkonto ein
            copies:
              - title: Bestimmen Sie, wie viele Sitzplätze Sie wünschen
                text: |
                  Ein selbstverwaltetes GitLab-Abonnement verwendet ein gleichzeitiges (Sitz-)Modell. Sie bezahlen ein Abonnement entsprechend der maximalen Benutzeranzahl im Abrechnungszeitraum. Sie können während des Abonnementzeitraums Benutzer hinzufügen und entfernen, solange die Gesamtzahl der Benutzer zu einem bestimmten Zeitpunkt die Anzahl der Abonnements nicht überschreitet.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-seats
                  text: Erfahren Sie, wie Sitzplätze ermittelt werden
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: Erhalten Sie Ihr selbstverwaltetes Abonnement
                text: |
                  Sie können Ihre eigene GitLab-Instanz installieren, verwalten und warten. Gehen Sie zur Preisseite und wählen Sie **Premium kaufen** oder **Ultimate kaufen**.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/self_managed/
                  text: Erfahren Sie mehr über Selbstverwaltung
                  ga_name: Learn more about self-managed
                  ga_location: body
              - title: Aktivieren Sie GitLab Enterprise Edition
                text: |
                  Wenn Sie eine neue GitLab-Instanz ohne Lizenz installieren, sind nur kostenlose Funktionen aktiviert. Um weitere Funktionen in GitLab Enterprise Edition (EE) zu aktivieren, aktivieren Sie Ihre Instanz mit dem beim Kauf bereitgestellten Aktivierungscode. Den Aktivierungscode finden Sie in der Kaufbestätigungs-E-Mail oder im Kundenportal unter „Einkäufe verwalten.
                link:
                  href: https://docs.gitlab.com/ee/user/admin_area/license.html
                  text: Aktivierungs details
                  ga_name: Activate GitLab Enterprise Edition
                  ga_location: body
              - title: Überprüfen Sie die Systemanforderungen
                text: |
                  <p>Sehen Sie sich die <a href="https://docs.gitlab.com/ee/install/requirements.html" data-ga-name="system rqeuirements" data-ga-location="body">unterstützten Betriebssysteme und die Mindestanforderungen </a> an, die für die Installation und Verwendung von GitLab erforderlich sind.</p>
              - title: Installieren Sie GitLab
                text: |
                  <p>Wählen Sie Ihre <a href="https://docs.gitlab.com/ee/install/#choose-the-installation-method" data-ga-name="Installation Method" data-ga-location="body">Installationsmethode</a>.</p>
                  <p>Installieren Sie es bei <a href="https://docs.gitlab.com/ee/install/#install-gitlab-on-cloud-providers\" data-ga-name="your cloud provider" data-ga-location="body">Ihrem Cloud-Anbieter</a> (falls zutreffend).</p>
              - title: Konfigurieren Sie Ihre Instanz
                text: |
                  Dazu gehören Dinge wie das Verbinden Ihrer E-Mail mit GitLab für Benachrichtigungen, das Einrichten des Abhängigkeits-Proxys, damit Sie Container-Images von Docker Hub für schnellere und zuverlässigere Builds zwischenspeichern können, das Festlegen von Authentifizierungsanforderungen und mehr.
                link:
                  href: https://docs.gitlab.com/ee/install/next_steps.html
                  text: Sehen Sie, was Sie konfigurieren können
                  ga_name: Configure your self-managed instance
                  ga_location: body
              - title: Offline-Umgebung einrichten (optional)
                text: |
                  Richten Sie eine Offline-Umgebung ein, wenn eine Isolierung vom öffentlichen Internet erforderlich ist (typischerweise anwendbar auf regulierte Branchen).
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html
                  text: Ist die Offline-Umgebung das Richtige für Sie?
                  ga_name: Set up off-line environment
                  ga_location: body
              - title: Erwägen Sie, die zulässigen Minuten für gemeinsam genutzte CI/CD-Runner zu begrenzen
                text: |
                  Um die Ressourcennutzung auf selbstverwalteten GitLab-Instanzen zu steuern, kann das Kontingent an Minuten berechnen für jeden Namespace von Administratoren festgelegt werden.
                link:
                  href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                  text: Mehr erfahren
                  ga_name: Consider limiting CI/CD shared runner minutes allowed
                  ga_location: body
              - title: Installieren Sie den GitLab-Runner
                text: |
                  GitLab Runner kann unter GNU/Linux, macOS, FreeBSD und Windows installiert und verwendet werden. Sie können es in einem Container installieren, indem Sie eine Binärdatei manuell herunterladen oder ein Repository für rpm/deb-Pakete verwenden.
                link:
                  href: https://docs.gitlab.com/runner/install/
                  text: Bewerten Sie Installationsoptionen
                  ga_name: Install GitLab runner
                  ga_location: body
              - title: GitLab-Runner konfigurieren (optional)
                text: |
                  GitLab Runner kann entsprechend Ihren Anforderungen und Richtlinien konfiguriert werden.
                link:
                  href: https://docs.gitlab.com/runner/configuration/
                  text: Siehe Runner-Konfigurationsoptionen
                  ga_name: Configure GitLab runner
                  ga_location: body
              - title: Selbstverwaltung
                text: |
                  Selbstverwaltung erfordert Selbstverwaltung. Als Administrator können Sie viele Dinge an Ihre individuellen Bedürfnisse anpassen.
                link:
                  href: https://docs.gitlab.com/ee/administration/#configuring-gitlab
                  text: Erfahren Sie mehr über Selbstverwaltung
                  ga_name: Self Administration
                  ga_location: body
          - title: Anwendungen integrieren (optional)
            copies:
              - text: |
                  Sie können Funktionen wie Geheimnisverwaltung oder Authentifizierungsdienste hinzufügen oder etablierte Anwendungen wie Issue-Tracker integrieren.
                link:
                  href: https://docs.gitlab.com/ee/integration/
                  text: Erfahren Sie mehr über Integrationen
                  ga_name: Integrate applications
                  ga_location: body
      - header: Mit GitLab
        show: Zeige alles
        hide: Alles verstecken
        id: using-gitlab
        items:
          - title: Richten Sie Ihre Organisation ein
            copies:
              - text: |
                  Konfigurieren Sie Ihre Organisation und ihre Benutzer. Bestimmen Sie Benutzerrollen und gewähren Sie jedem Zugriff auf die Projekte, die er benötigt.
                link:
                  href: https://docs.gitlab.com/ee/topics/set_up_organization.html
                  text: Mehr erfahren
                  ga_name: Setup your organization
                  ga_location: body
          - title: Organisieren Sie die Arbeit mit Projekten
            copies:
              - text: |
                  In GitLab können Sie Projekte erstellen, um Ihre Codebasis zu hosten. Sie können Projekte auch verwenden, um Probleme zu verfolgen, Arbeit zu planen, am Code zusammenzuarbeiten und kontinuierlich zu erstellen, zu testen und das integrierte CI/CD zum Bereitstellen Ihrer App zu verwenden.
                link:
                  href: https://docs.gitlab.com/ee/user/project/index.html
                  text: Mehr erfahren
                  ga_name: Organize work with projects
                  ga_location: body
          - title: Planen und verfolgen Sie die Arbeit
            copies:
              - text: |
                  Planen Sie Ihre Arbeit, indem Sie Anforderungen, Probleme und Epics erstellen. Planen Sie die Arbeit mit Meilensteinen und verfolgen Sie die Zeit Ihres Teams. Erfahren Sie, wie Sie mit schnellen Aktionen Zeit sparen, sehen Sie, wie GitLab Markdown-Text rendert, und erfahren Sie, wie Sie Git für die Interaktion mit GitLab verwenden.
                link:
                  href: https://docs.gitlab.com/ee/topics/plan_and_track.html
                  text: Mehr erfahren
                  ga_name: Plan and track work
                  ga_location: body
          - title: Erstellen Sie Ihre Anwendung
            copies:
              - text: |
                  Fügen Sie Ihren Quellcode einem Repository hinzu, erstellen Sie Zusammenführungsanforderungen, um Code einzuchecken, und verwenden Sie CI/CD, um Ihre Anwendung zu generieren.
                link:
                  href: https://docs.gitlab.com/ee/topics/build_your_application.html
                  text: Mehr erfahren
                  ga_name: Build your application
                  ga_location: body
          - title: Sichern Sie Ihre Bewerbung
            copies:
              - title: Bestimmen Sie, welche Scanner Sie verwenden möchten
                text: |
                  GitLab bietet Secrets-Erkennung, SAST und Container-Scanning im kostenlosen Kontingent. DAST, Abhängigkeits- und IaC-Scanning, API-Sicherheit, Lizenz-Compliance und Fuzzing sind in der Ultimate-Stufe verfügbar. Alle Scanner sind standardmäßig aktiviert. Sie können sie einzeln deaktivieren.
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/configuration/
                  text: Konfigurieren Sie die Scannerverwendung
                  ga_name: Determine which scanners to use
                  ga_location: body
              - title: Konfigurieren Sie Ihre Sicherheitsrichtlinien
                text: |
                  Richtlinien in GitLab bieten Sicherheitsteams die Möglichkeit, die Ausführung von Scans ihrer Wahl zu verlangen, wenn eine Projektpipeline gemäß der angegebenen Konfiguration ausgeführt wird. Sicherheitsteams können daher sicher sein, dass die von ihnen eingerichteten Scans nicht geändert, verändert oder deaktiviert wurden. Es können Richtlinien für die Scan-Ausführung und für die Scan-Ergebnisse festgelegt werden.
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/policies/
                  text: Konfigurieren Sie Sicherheitsrichtlinien
                  ga_name: Configure your security policies
                  ga_location: body
              - title: Konfigurieren Sie Genehmigungsregeln für Zusammenführungsanfragen
                text: |
                  Sie können Ihre Zusammenführungsanfragen so konfigurieren, dass sie genehmigt werden müssen, bevor sie zusammengeführt werden können. Während GitLab Free allen Benutzern mit Entwickler- oder höheren Berechtigungen erlaubt, Zusammenführungsanfragen zu genehmigen, sind diese Genehmigungen optional. GitLab Premium und GitLab Ultimate bieten zusätzliche Flexibilität, um detailliertere Kontrollen festzulegen. MR-Genehmigungen auf Projektbasis und auf Gruppenebene. Administratoren der selbstverwalteten GitLab-Instanzen GitLab Premium und GitLab Ultimate können auch Genehmigungen für die gesamte Instanz konfigurieren.
                link:
                  href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
                  text: Erfahren Sie mehr über die MR-Genehmigungsregeln
                  ga_name: Configure MR approval rules
                  ga_location: body
          - title: Stellen Sie Ihre Anwendung bereit und geben Sie sie frei
            copies:
              - text: |
                  Stellen Sie Ihre Anwendung intern oder öffentlich bereit. Verwenden Sie Flags, um Funktionen schrittweise freizugeben.
                link:
                  href: https://docs.gitlab.com/ee/topics/release_your_application.html
                  text: Mehr erfahren
                  ga_name: Deploy and release your application
                  ga_location: body
          - title: Überwachen Sie die Anwendungsleistung
            copies:
              - text: |
                 GitLab bietet eine Vielzahl von Tools, die Sie beim Betrieb und der Wartung Ihrer Anwendungen unterstützen.  Sie können die Kennzahlen verfolgen, die für Ihr Team am wichtigsten sind, automatische Warnungen generieren, wenn die Leistung nachlässt, und diese Warnungen verwalten – alles in GitLab.
                link:
                  href: https://docs.gitlab.com/ee/operations/index.html
                  text: Mehr erfahren
                  ga_name: Monitor application performance
                  ga_location: body
          - title: Überwachen Sie die Läuferleistung
            copies:
              - text: |
                  GitLab verfügt über ein eigenes System zur Messung der Anwendungsleistung. GitLab Performance Monitoring ermöglicht die Messung verschiedenster Statistiken.
                link:
                  href: https://docs.gitlab.com/runner/monitoring/index.html
                  text: Mehr erfahren
                  ga_name: Monitor runner performance
                  ga_location: body
          - title: Verwalten Sie Ihre Infrastruktur
            copies:
              - text: |
                  GitLab bietet verschiedene Funktionen, um Ihre Infrastrukturverwaltungspraktiken zu beschleunigen und zu vereinfachen.
                  * GitLab verfügt über umfassende Integrationen mit Terraform für die Bereitstellung von Cloud-Infrastrukturen, die Ihnen einen schnellen Einstieg ohne Einrichtung ermöglichen, bei Infrastrukturänderungen in Zusammenführungsanfragen auf die gleiche Weise wie bei Codeänderungen zusammenarbeiten und mithilfe einer Modulregistrierung skalieren können.
                  * Die GitLab-Integration mit Kubernetes hilft Ihnen bei der Installation, Konfiguration, Verwaltung, Bereitstellung und Fehlerbehebung von Clusteranwendungen.
                link:
                  href: https://docs.gitlab.com/ee/user/infrastructure/index.html
                  text: Mehr erfahren
                  ga_name: Manage your infrastructure
                  ga_location: body
          - title: Analysieren Sie die GitLab-Nutzung
            copies:
              - text: |
                  GitLab bietet Analysen auf Projekt-, Gruppen- und Instanzebene. Das DevOps Research and Assessment (DORA)-Team hat mehrere Schlüsselmetriken entwickelt, die Sie als Leistungsindikatoren für Softwareentwicklungsteams verwenden können. GitLab Ultimate hat sie eingebunden.
                link:
                  href: https://docs.gitlab.com/ee/user/analytics/index.html
                  text: Mehr erfahren
                  ga_name: Analyze GitLab usage
                  ga_location: body
  next_steps:
    header: Bringen Sie Ihr Kleinunternehmen auf den nächsten Schritt
    cards:
      - title: Benötigen Sie Kundensupport?
        text: GitLab [Dokumentation](https://docs.gitlab.com/){data-ga-name="documentation" data-ga-location="body"} kann Ihre Fragen beantworten.
        avatar: /nuxt-images/icons/avatar_orange.png
        col_size: 4
        link:
          text: Kundendienst
          url: /support/
          data_ga_name: Contact support
          data_ga_location: body
      - title: Benötigen Sie weitere Hilfe?
        text: GitLab Professional Services kann Ihnen beim Einstieg, bei der Integration in Anwendungen von Drittanbietern und bei der Migration von anderen Tools helfen.
        avatar: /nuxt-images/icons/avatar_pink.png
        col_size: 4
        link:
          text: Bitten Sie meinen PS, mich zu kontaktieren
          url: /sales/
          data_ga_name: Have my PS contact me
          data_ga_location: body
      - title: Möchten Sie lieber mit einem Vertriebspartner zusammenarbeiten?
        text: Arbeiten Sie lieber mit einem Distributor, Integrator oder Managed Service Provider (MSP) zusammen? Sie können GitLab auch über die Marktplätze unserer [Cloud-Partner](https://about.gitlab.com/partners/technology-partners/){data-ga-name="cloud Partners" data-ga-location="body"} kaufen.
        avatar: /nuxt-images/icons/avatar_blue.png
        col_size: 4
        link:
          text: Siehe Vertriebspartnerverzeichnis
          url: https://partners.gitlab.com/English/directory/
          data_ga_name: See channel partner directory
          data_ga_location: body
      - title: Erwägen Sie ein Upgrade?
        text: Erfahren Sie mehr über die Vorteile von [Premium](/pricing/premium/){data-ga-name="why premium" data-ga-location="body"} und [Ultimate](/pricing/ultimate/){data-ga-name="why supreme" data-ga-location="body"}.
        col_size: 6
        link:
          text: Warum Ultimate
          url: /pricing/ultimate
          data_ga_name: Why ultimate
          data_ga_location: body
      - title: Erwägen Sie die Integration eines Drittanbieters?
        text: Der offene Kern von GitLab erleichtert die Integration. Wir haben viele Technologiepartner, die die umfassende DevSecOps-Plattform von GitLab ergänzen, um den individuellen Anforderungen und Anwendungsfällen gerecht zu werden.
        col_size: 6
        link:
          text: Sehen Sie sich unsere Allianz- und Technologiepartner an
          url: /partners/technology-partners/
          data_ga_name: See our Alliance and Technology partners
          data_ga_location: body
