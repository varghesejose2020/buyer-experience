---
  title: GitLab herunterladen und installieren
  description: Lade deine eigene GitLab-Instanz herunter, installiere und verwalte sie mit verschiedenen Installationspaketen und Downloads für Linux, Kubernetes, Docker, Google Cloud und mehr.
  side_menu:
    anchors:
      text: "Auf dieser Seite"
      data:
      - text: Offizielles Linux-Paket
        href: "#official-linux-package"
        data_ga_name: official linux package
        data_ga_location: side menu
      - text: Kubernetes-Bereitstellungen
        href: "#kubernetes-deployments"
        data_ga_name: kubernetes deployments
        data_ga_location: side menu
      - text: Unterstützte Cloud
        href: "#supported-cloud"
        data_ga_name: supported cloud
        data_ga_location: side menu
      - text: Andere offizielle Methoden
        href: "#other-official-methods"
        data_ga_name: other official methods
        data_ga_location: side menu
      - text: Community-Beiträge
        href: "#community-contributed"
        data_ga_name: community contributed
        data_ga_location: side menu
      - text: Schon installiert?
        href: "#already-installed"
        data_ga_name: already installed
        data_ga_location: side menu
    hyperlinks:
      text: "Mehr zu diesem Thema"
      data:
        - text: "Hol dir die kostenlose Ultimate-Testversion"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "free trial"
          data_ga_location: "side-navigation"
        - text: "Self-Managed installieren"
          href: "/free-trial/"
          data_ga_name: "self managed trial"
          data_ga_location: "side-navigation"
        - text: "Erste Schritte mit SaaS"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "saas get started"
          data_ga_location: "side-navigation"
        - text: "Kauf auf Marketplaces"
          href: "https://page.gitlab.com/cloud-partner-marketplaces.html"
          data_ga_name: "partner marketplace"
          data_ga_location: "side-navigation"
  header:
    title: Self-Managed GitLab installieren
    subtitle: Probiere GitLab noch heute aus. Du kannst deine eigene GitLab-Instanz herunterladen, installieren und verwalten.
    text: |
      [Testversion](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="free trial" data-ga-location="header"} – Starte noch heute deine kostenlose Ultimate Testversion

      [Self-Managed](/free-trial/){data-ga-name="self managed trial" data-ga-location="header"} – Installation auf deiner eigenen Infrastruktur

      [SaaS](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="saas get started" data-ga-location="header"} – Erste Schritte mit unserem SaaS-Angebot

      [Marketplace](https://page.gitlab.com/cloud-partner-marketplaces.html){data-ga-name="partner marketplace" data-ga-location="header"} – Nahtlos einkaufen über den Cloud Marketplace deiner Wahl
  linux:
    title: Offizielles Linux-Paket
    subtitle: Empfohlene Installationsmethode
    text: |
      Diese Methode wird für den Einstieg empfohlen. Die Linux-Pakete sind technisch ausgereift, skalierbar und werden heute auf GitLab.com verwendet. Wenn du zusätzliche Flexibilität und Ausfallsicherheit benötigst, empfehlen wir, GitLab wie in der [Referenzarchitekturdokumentation] (https://docs.gitlab.com/ee/administration/reference_architectures/index.html){data-ga-name="reference architecture documentation" data-ga-location="linux installation"} beschrieben zu installieren.

      Die Installation unter Linux ist schneller, einfacher zu aktualisieren und enthält Funktionen zur Verbesserung der Zuverlässigkeit, die in anderen Methoden nicht enthalten sind. Die Installation erfolgt über ein einziges Paket (auch als Omnibus bekannt), das alle für die Ausführung von GitLab erforderlichen Dienste und Tools bündelt. Es werden mindestens 4 GB RAM empfohlen ([Mindestanforderungen](https://docs.gitlab.com/ee/install/requirements.html){data-ga-name="installation requirements documentation" data-ga-location="linux installation"}).

      In unserem Repository für Pakete ([GitLab-ee](https://packages.gitlab.com/gitlab/gitlab-ee) or [GitLab-ce](https://packages.gitlab.com/gitlab/gitlab-ce)) kannst du überprüfen, ob die erforderliche GitLab-Version für die Version des Host-Betriebssystems verfügbar ist.
    cards:
      - title: Ubuntu
        id: ubuntu
        subtext: 18.04 LTS, 20.04 LTS, 22.04 LTS
        icon:
          name: ubuntu-purple
          alt: Ubuntu-Symbol
        link_text: Installationsanweisungen anzeigen +
        link_url: '#ubuntu'
        data_ga_name: ubuntu installation documentation
        data_ga_location: linux installation
      - title: Debian
        id: debian
        subtext: 10, 11
        icon:
          name: debian
          alt: Debian-Symbol
        link_text: Installationsanweisungen anzeigen +
        link_url: '#debian'
        data_ga_name: debian installation documentation
        data_ga_location: linux installation
      - title: AlmaLinux
        id: almalinux
        subtext: Versionen 8, 9 für RHEL-kompatible Distributionen
        icon:
          name: almalinux
          alt: AlmaLinux-Symbol
        link_text: Installationsanweisungen anzeigen +
        link_url: '#almalinux'
        data_ga_name: almalinux installation documentation
        data_ga_location: linux installation
      - title: CentOS 7
        id: centos-7
        subtext: und RHEL, Oracle, Scientific
        icon:
          name: centos
          alt: CentOS-Symbol
        link_text: Installationsanweisungen anzeigen +
        link_url: '#centos-7'
        data_ga_name: centos 7 installation documentation
        data_ga_location: linux installation
      - title: OpenSUSE Leap
        id: opensuse-leap
        subtext: OpenSUSE Leap 15.4, 15.5, und SUSE Linux Enterprise Server 12.2, 12.5
        icon:
          name: opensuse
          alt: OpenSuse-Symbol
        link_text: Installationsanweisungen anzeigen +
        link_url: '#opensuse-leap'
        data_ga_name: opensuse leap installation documentation
        data_ga_location: linux installation
      - title: Amazon Linux 2
        id: amazonlinux-2
        subtext:
        icon:
          name: aws
          alt: AWS-Symbol
        link_text: Installationsanweisungen anzeigen +
        link_url: '#amazonlinux-2'
        data_ga_name: amazonlinux 2 installation documentation
        data_ga_location: linux installation

      - title: Raspberry Pi OS
        id: raspberry-pi-os
        subtext: Bullseye und Buster (32 Bit)
        icon:
          name: raspberry-pi
          alt: 'Symbol: Raspberry Pi'
        link_text: Installationsanweisungen anzeigen +
        link_url: '#raspberry-pi-os'
        data_ga_name: raspberry pi os installation documentation
        data_ga_location: linux installation
    dropdowns:
      - id: ubuntu
        tip: |
          Für Ubuntu 20.04 und 22.04 sind auch „arm64“-Pakete verfügbar und werden auf dieser Plattform automatisch verwendet, wenn du das GitLab-Repository zur Installation verwendest.
        first_step: 1. Installieren und konfigurieren der notwendigen Abhängigkeiten
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
          ```
        postfix_command: |
          ```
           sudo apt-get install -y postfix
          ```
        download_command: |
          ```
           curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
           sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        link_back: ubuntu
      - id: debian
        tip: |
          Für Debian 10 sind auch „arm64“-Pakete verfügbar und werden auf dieser Plattform automatisch verwendet, wenn du das GitLab-Repository für die Installation verwendest.
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates perl
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
            curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        line_back: debian
      - id: almalinux
        tip: |
          Für AlmaLinux und RedHat Version 8 und 9 sind auch „arm64“-Pakete verfügbar und werden auf dieser Plattform automatisch verwendet, wenn du das GitLab-Repository zur Installation verwendest.
        dependency_text: Unter AlmaLinux (und RedHat) Version 8 und 9 öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugang in der Systemfirewall. Dies ist ein optionaler Schritt, den du überspringen kannst, wenn du nur von deinem lokalen Netzwerk aus auf GitLab zugreifen möchtest.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: almalinux
      - id: centos-7
        dependency_text: Unter CentOS 7 (und RedHat/Oracle/Scientific Linux 7) öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugang in der Systemfirewall. Dies ist ein optionaler Schritt, den du überspringen kannst, wenn du nur von deinem lokalen Netzwerk aus auf GitLab zugreifen möchtest.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: centos-7
      - id: opensuse-leap
        tip: |
          Für OpenSuse sind auch „arm64“-Pakete verfügbar und werden auf dieser Plattform automatisch verwendet, wenn du das GitLab-Repository für die Installation verwendest.
        dependency_text: Unter OpenSUSE öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugang in der Systemfirewall. Dies ist ein optionaler Schritt, den du überspringen kannst, wenn du nur von deinem lokalen Netzwerk aus auf GitLab zugreifen möchtest.
        dependency_command: |
          ```
          sudo zypper install curl openssh perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo zypper install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash```
        install_command: |
          ```sudo EXTERNAL_URL="https://gitlab.example.com" zypper install gitlab-ee```
        line_back: opensuse-leap

      - id: amazonlinux-2
        tip: |
          Für Amazon Linux 2 sind auch „arm64“-Pakete verfügbar und werden auf dieser Plattform automatisch verwendet, wenn du das GitLab-Repository für die Installation verwendest.
        dependency_text: Unter Amazon Linux 2 öffnen die folgenden Befehle auch den HTTP-, HTTPS- und SSH-Zugang in der Systemfirewall. Dies ist ein optionaler Schritt, den du überspringen kannst, wenn du nur von deinem lokalen Netzwerk aus auf GitLab zugreifen möchtest.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server openssh-clients perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: amazonlinux-2
      - id: raspberry-pi-os
        tip: |
          Es wird ein Raspberry Pi 4 mit mindestens 4 GB empfohlen. Zur Zeit wird nur 32 Bit (armhf) unterstützt. Die Unterstützung für 64 Bit („arm64“) kommt in Kürze.
        dependency_command: |
          ```
          sudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
          curl https://packages.gitlab.com/gpg.key | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
          sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ce
          ```
  kubernetes:
    title: Kubernetes-Bereitstellungen
    text: |
      Bei der Installation von GitLab auf Kubernetes gibt es einige Kompromisse, die du beachten musst:

        - Administration und Problembehandlung erfordern Kubernetes-Kenntnisse
        - Bei kleineren Installationen kann es teurer werden. Die Standardinstallation erfordert mehr Ressourcen als die Bereitstellung von Linux-Paketen auf einem einzelnen Knoten, da die meisten Dienste redundant bereitgestellt werden.
        - Es gibt einige [Einschränkungen, die du berücksichtigen solltest.](https://docs.gitlab.com/charts/#limitations){data-ga-name="chart limitations" data-ga-location="kubernetes installation"}

      Verwende diese Methode, wenn deine Infrastruktur auf Kubernetes aufbaut und du mit dessen Funktionsweise vertraut bist. Die Methoden für die Verwaltung, die Beobachtbarkeit und einige Konzepte sind anders als bei herkömmlichen Bereitstellungen. Die Helm-Diagramm-Methode ist für Vanilla Kubernetes-Bereitstellungen gedacht und der GitLab Operator kann für die Bereitstellung von GitLab auf einem OpenShift-Cluster verwendet werden. Der GitLab Operator kann verwendet werden, um Day-2-Vorgänge sowohl in OpenShift- als auch in Vanilla-Kubernetes-Bereitstellungen zu automatisieren.
    cards:
      - title: Helm-Diagramm
        subtext: Installation von GitLab mithilfe von HELM-Diagrammen
        icon:
          name: kubernetes-purple
          alt: Kubernetes-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/charts/
        data_ga_name: helm charts
        data_ga_location: kubernetes installation
      - title: GitLab-Operator
        new_flag: true
        subtext: Installation von GitLab mithilfe des Operators
        icon:
          name: gitlab-operator
          alt: GitLab-Operator-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/charts/installation/operator.html
        data_ga_name: gitlab operator
        data_ga_location: kubernetes installation
  supported_cloud:
    title: Unterstützte Cloud
    text: |
      Verwende das offizielle Linux-Paket, um GitLab bei verschiedenen Cloud-Anbietern zu installieren.
    cards:
      - title: Amazon Web Services (AWS)
        subtext: GitLab auf AWS installieren
        icon:
          name: aws
          alt: AWS-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/aws/
        data_ga_name: aws install documentation
        data_ga_location: supported cloud
      - title: Google Cloud Platform (GCP)
        subtext: GitLab auf der GCP installieren
        icon:
          name: gcp
          alt: 'Symbol: GCP'
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/google_cloud_platform/
        data_ga_name: gcp install documentation
        data_ga_location: supported cloud
      - title: Microsoft Azure
        subtext: GitLab auf Azure installieren
        icon:
          name: azure
          alt: Azure-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/azure/
        data_ga_name: azure install documentation
        data_ga_location: supported cloud
  official_methods:
    title: Andere offizielle, unterstützte Installationsmethoden
    cards:
      - title: Docker
        subtext: Offizielle GitLab Docker-Images
        icon:
          name: docker
          alt: Docker-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/docker.html
        data_ga_name: docker install documentation
        data_ga_location: official installation
      - title: Referenz-architekturen
        subtext: Empfohlene GitLab-Bereitstellungstopologien
        icon:
          name: gitlab-tanuki
          alt: GitLab-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/index.html
        data_ga_name: reference architectures install documentation
        data_ga_location: official installation
      - title: Installation aus Quelldateien
        subtext: Installation von GitLab unter Verwendung der Quelldateien auf einem Debian/Ubuntu-System
        icon:
          name: source
          alt: 'Symbol: Quelldatei'
          variant: marketing
          hex_color: '#336CE4'          
        link_text: Installationsanweisungen anzeigen
        link_url: https://docs.gitlab.com/ee/install/installation.html
        data_ga_name: installation from source
        data_ga_location: official installation
      - title: GitLab Environment Toolkit (GET)
        subtext: Automatisierung für die Bereitstellung von GitLab-Referenzarchitekturen mit Terraform und Ansible
        icon:
          name: gitlab-environment-toolkit
          alt: GitLab Environment Toolkit-Symbol
          variant: marketing
        link_text: Installationsanweisungen anzeigen
        link_url: https://gitlab.com/gitlab-org/gitlab-environment-toolkit
        data_ga_name: gitlab environment toolkit installation
        data_ga_location: official installation
  unofficial_methods:
    title: Inoffizielle, nicht unterstützte Installationsmethoden
    cards:
      - title: Debian-natives Paket
        subtext: von Pirate Praveen
        icon:
          name: debian
          alt: Debian-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://wiki.debian.org/gitlab/
        data_ga_name: debian native installation
        data_ga_location: unofficial installation
      - title: FreeBSD-Paket
        subtext: von Torsten Zühlsdorff
        icon:
          name: freebsd
          alt: FreeBSD-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: http://www.freshports.org/www/gitlab-ce
        data_ga_name: freebsd-installation
        data_ga_location: unofficial installation
      - title: Arch Linux-Paket
        subtext: von der Arch Linux Community
        icon:
          name: arch-linux
          alt: Arch Linux-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://archlinux.org/packages/extra/x86_64/gitlab/
        data_ga_name: arch linux installation
        data_ga_location: unofficial installation
      - title: Puppet-Modul
        subtext: von Vox Pupuli
        img_src: /nuxt-images/install/puppet-logo.svg
        icon:
          name: puppet
          alt: Puppet-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://forge.puppet.com/puppet/gitlab
        data_ga_name: puppet module installation
        data_ga_location: unofficial installation
      - title: Ansible Playbook
        subtext: von Jeff Geerling
        icon:
          name: ansible-purple
          alt: Ansible-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://github.com/geerlingguy/ansible-role-gitlab
        data_ga_name: ansible installation
        data_ga_location: unofficial installation
      - title: Virtuelle GitLab-Appliance (KVM)
        subtext: von OpenNebula
        icon:
          name: open-nebula
          alt: Open Nebula-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://marketplace.opennebula.io/appliance/6b54a412-03a5-11e9-8652-f0def1753696
        data_ga_name: opennebula installation
        data_ga_location: unofficial installation
      - title: GitLab auf Cloudron
        subtext: über die Cloudron App-Bibliothek
        img_src: /nuxt-images/install/cloudron-logo.svg
        icon:
          name: cloudron
          alt: Cloudron-Symbol
        link_text: Installationsanweisungen anzeigen
        link_url: https://cloudron.io/store/com.gitlab.cloudronapp.html
        data_ga_name: cloudron installation
        data_ga_location: unofficial installation
  updates:
    title: Du hast GitLab bereits installiert?
    cards:
      - title: Update von einer alten Version von GitLab
        text: Aktualisiere deine GitLab-Installation, um die neuesten Funktionen zu nutzen. Am 22. jedes Monats werden neue Versionen von GitLab veröffentlicht, die neue Funktionen enthalten.
        link_url: https://about.gitlab.com/update/
        link_text: Update auf die neueste Version von GitLab
        data_ga_name: Update to the latest relase of GitLab
        data_ga_location: update
      - title: Update von der GitLab Community Edition
        text: Die GitLab Enterprise Edition enthält erweiterte Funktionen, die in der Community Edition nicht verfügbar sind.
        link_url: https://about.gitlab.com/upgrade/
        link_text: Upgrade auf die Enterprise Edition
        data_ga_name: Upgrade to Enterprise Edition
        data_ga_location: update
      - title: Upgrade von manuell installiertem Omnibus-Paket
        text: Mit GitLab 7.10 haben wir Paket-Repositorys für GitLab eingeführt, mit denen du GitLab mit einem einfachen Befehl installieren kannst.
        link_url: https://about.gitlab.com/upgrade-to-package-repository/
        link_text: Upgrade zum Omnibus-Paket-Repository
        data_ga_name: Upgrade to Omnibus package repository
        data_ga_location: update
      - title: Anwendungen von Drittanbietern, die GitLab unterstützen
        text: GitLab ist offen für Zusammenarbeit und engagiert sich für den Aufbau von Technologiepartnerschaften im DevOps-Ökosystem. Erfahre mehr über die Vorteile und Voraussetzungen, um GitLab-Technologiepartner zu werden.
        link_url: /partners/
        link_text: Drittanbieter-Anwendungen ansehen
        data_ga_name: View third-party applications
        data_ga_location: update
