---
  title: GitLab pour les services financiers
  description: Le codage social, l'intégration continue et l'automatisation des releases ont fait leurs preuves pour accélérer le développement et la qualité des logiciels afin d'atteindre les objectifs de la mission. En savoir plus!
  twitter_image: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  og_image: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  image_title: /nuxt-images/open-graph/Opengraph-GitLab-finances.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab pour les services financiers
        subtitle: La plateforme DevSecOps destinée aux institutions financières innovantes
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Essayer Ultimate gratuitement
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: En savoir plus sur la tarification
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
          image_url_mobile: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
          alt: "Image: GitLab pour les services financiers"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: Goldman Sachs
            image: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
            url: /customers/goldman-sachs/
            aria_label: Lien vers l'étude de cas de Goldman Sachs
          - name: Bendigo and Adelaide Bank
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: /customers/bab/
            aria_label: Lien vers l'étude de cas Bendigo and Adelaide Bank
          - name: Credit Agricole
            image: /nuxt-images/customers/credit-agricole-logo-2.png
            url: /customers/credit-agricole/
            aria_label: Lien vers l'étude de cas du Crédit Agricole
          - name: Worldline
            image: /nuxt-images/logos/worldline-logo-mono.png
            url: /customers/worldline/
            aria_label: Lien vers l'étude de cas Worldline
          - name: Moneyfarm
            image: /nuxt-images/logos/moneyfarm_logo.png
            url: /customers/moneyfarm/
            aria_label: Lien vers l'étude de cas Moneyfarm
          - name: UBS
            image: /nuxt-images/home/logo_ubs_mono.svg
            url: /blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: Lien vers l'étude de cas d'UBS
    - name: 'side-navigation-variant'
      links:
        - title: Présentation
          href: '#overview'
        - title: Témoignages
          href: '#testimonials'
        - title: Capacités
          href: '#capabilities'
        - title: Avantages
          href: '#benefits'
        - title: Études de cas
          href: '#case-studies'
      slot_enabled: true
      slot_offset: 2
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Sécurité. Vitesse. Efficacité.
                is_accordion: false
                items:
                  - icon:
                      name: devsecops
                      alt: Icône DevSecOps
                      variant: marketing
                    header: Réduction des risques de sécurité et de conformité
                    text: Détectez les vulnérabilités et les problèmes de sécurité en amont du processus de développement et appliquez des politiques afin de respecter les exigences de conformité.
                    link_text: En savoir plus sur DevSecOps
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: increase
                      alt: Icône d'augmentation
                      variant: marketing
                    header: Une mise sur le marché plus rapide
                    text: Améliorez l'expérience de vos clients et proposez-leur de nouvelles fonctionnalités plus rapidement en automatisant votre processus de livraison de logiciels à l'aide d'une plateforme DevSecOps complète.
                    link_text: En savoir plus
                    link_url: /platform/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: piggy-bank-alt
                      alt: Icône de tirelire en forme de petit cochon
                      variant: marketing
                    header: Des coûts opérationnels réduits
                    text: Réduisez la complexité de la chaîne d'outils et augmentez la collaboration, la productivité et l'innovation en optant pour une solution tout-en-un.
                    link_text: Calculer vos économies de coûts
                    link_url: /calculator/roi/
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                header: |
                  Approuvée par le secteur des services financiers.
                  <br />
                  Adorée par les développeurs.
                quotes:
                  - title_img:
                      url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
                      alt: goldman sachs
                    quote: GitLab nous a permis d'augmenter considérablement la vélocité de notre développement au sein de notre division Ingénierie. Nous constatons aujourd'hui que certaines équipes peuvent exécuter et fusionner plus de 1 000 compilations de branches de fonctionnalités CI par jour!
                    author: Andrew Knight
                    position: Directeur général de Goldman Sachs
                    ga_carousel: financial services goldman sachs
                  - title_img:
                      url: /nuxt-images/home/logo_ubs_mono.svg
                      alt: Logo UBS
                    quote: |
                      « Nous n'aurions pas pu avoir cette expérience transparente sans GitLab. GitLab nous a permis de devancer bon nombre de nos concurrents et d'éliminer les complexités entre le codage, les tests et le déploiement. »
                    author: Rick Carey
                    position: Group Chief Technology Officer chez UBS
                    ga_carousel: financial services UBS
                  - title_img:
                      url: /nuxt-images/logos/new10.svg
                      alt: Logo new10
                    quote: GitLab nous aide vraiment dans notre architecture très moderne, parce que GitLab prend en charge Kubernetes, le modèle d'application serverless ainsi que des fonctionnalités de sécurité sympa telles que DAST et SAST. Grâce à GitLab, nous disposons d'une architecture vraiment à la pointe de la technologie.
                    author: Kirill Kolyaskin
                    position: Directeur technique chez New10
                    ga_carousel: financial services new10
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: Une plateforme DevSecOps complète destinée aux services financiers
                sub_description: 'Faites en sorte que vos clients puissent répondre plus efficacement et en toute sécurité à leurs besoins financiers en vous aidant des fonctionnalités suivantes:'
                white_bg: true
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                alt: image de vitrine
                solutions:
                  - title: SBOM et dépendances
                    description: Bénéficiez d'une vue claire des dépendances de votre projet ou de la nomenclature logicielle, notamment les vulnérabilités connues ainsi que d'autres détails clés, tels que le nom du développeur chargé de la création du paquet et qui a installé chaque dépendance et sa licence logicielle.
                    icon:
                      name: less-risk
                      alt: Icône de risque moindre
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: sbom
                    data_ga_location: solutions block
                  - title: Analyse de sécurité complète
                    description: Détectez les vulnérabilités et sécurisez vos applications à l'aide d'outils de sécurité intégrés, notamment SAST, la détection de secrets, l'analyse des conteneurs, l'analyse des images de cluster, DAST, l'analyse IaC, le test à données aléatoires de l'API et bien plus encore.
                    icon:
                      name: monitor-pipeline
                      alt: Icône écran pipeline
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/
                    data_ga_name: Comprehensive security scanning
                    data_ga_location: solutions block
                  - title: Test à données aléatoires
                    description: Découvrez les bogues et les problèmes de sécurité potentiels que d'autres processus d'assurance qualité peuvent ne pas détecter en ajoutant à vos pipelines dans GitLab des tests à données aléatoires basés sur des critères de couverture.
                    icon:
                      name: monitor-test
                      alt: Icône écran test
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: Contrôles communs pour la conformité
                    description: Automatisez et appliquez des contrôles communs avec diverses fonctionnalités GitLab, des branches et environnements protégés aux approbations de requêtes de fusion, en passant par les journaux d'audit, la conservation des artefacts et l'analyse complète de la sécurité.
                    icon:
                      name: shield-check
                      alt: Icône de bouclier avec une coche
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://about.gitlab.com/solutions/financial-services-regulatory-compliance/
                    data_ga_name: controls for compliance
                    data_ga_location: solutions block
                  - title: Automatisation du flux de travail conforme
                    description: Assurez-vous que les paramètres configurés par votre équipe de conformité le restent de cette façon et qu'ils respectent les cadriciels et pipelines de conformité.
                    icon:
                      name: devsecops
                      alt: Icône DevSecOps
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Une solution unique adaptée aux services financiers
                cards:
                  - title: Analyse de sécurité intégrée
                    description: Donnez aux développeurs les moyens de trouver et de résoudre les tickets liés à la sécurité au fur et à mesure qu'ils sont créés. Utilisez les <a href="https://docs.gitlab.com/ee/user/application_security/">scanners intégrés</a> de GitLab, notamment SAST, DAST, l'analyse des conteneurs, l'analyse IaC et la détection de secret pour n'en citer que quelques-uns, et intégrez des scanners personnalisés.
                    icon:
                      name: monitor-test
                      alt: Icône écran test
                      variant: marketing
                  - title: Prise en charge de votre parcours Cloud
                    description: GitLab est conçu pour les applications cloud-natives et dispose d'intégrations étroites avec <a href="https://about.gitlab.com/solutions/kubernetes/">Kubernetes</a>, <a href="https://about.gitlab.com/partners/technology-partners/aws/">AWS</a>, <a href="https://about.gitlab.com/partners/technology-partners/google-cloud-platform/" >Google Cloud</a> et <a href="https://about.gitlab.com/partners/technology-partners/#cloud-partners">d'autres fournisseurs de services cloud</a>.
                    icon:
                      name: gitlab-cloud
                      alt: Icône de nuage avec le logo GitLab
                      variant: marketing
                  - title: Sur site, auto-hébergé ou SaaS
                    description: GitLab fonctionne dans tous les environnements. Le choix vous appartient.
                    icon:
                      name: monitor-gitlab
                      alt: Icône d'écran affichant le logo GitLab
                      variant: marketing

        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: Exemples de réussite dans le secteur de la finance
                link:
                  text: Toutes les études de cas
                rows:
                  - title: Goldman Sachs
                    subtitle: Découvrez comment des dizaines d'équipes de Goldman Sachs poussent leurs applications en production en moins de 24 heures
                    image:
                      url: /nuxt-images/blogimages/Goldman_Sachs_case_study.jpg
                      alt: sommets de bâtiments vus depuis la rue
                    button:
                      href: https://about.gitlab.com/customers/goldman-sachs/
                      text: En savoir plus
                      data_ga_name: goldman sachs learn more
                      data_ga_location: case studies
                  - title: Moneyfarm
                    subtitle: Cette société européenne de gestion de patrimoine a amélioré l'expérience de ses développeurs et doublé ses déploiements
                    image:
                      url: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
                      alt: reflets dans les fenêtres de bâtiments
                    button:
                      href: https://about.gitlab.com/customers/moneyfarm/
                      text: En savoir plus
                      data_ga_name: uw learn more
                      data_ga_location: case studies
                  - title: Bendigo and Adelaide Bank
                    subtitle: Découvrez comment l'une des banques les plus réputées d'Australie a adopté la technologie Cloud et amélioré son efficacité opérationnelle
                    image:
                      url: /nuxt-images/blogimages/bab_cover_image.jpg
                      alt: bâtiments en noir et blanc
                    button:
                      href: https://about.gitlab.com/customers/bab/
                      text: En savoir plus
                      data_ga_name: bendigo and adelaide learn more
                      data_ga_location: case studies
    - name: 'by-solution-link'
      data:
        title: 'Événements GitLab'
        description: "Participez à un événement pour découvrir comment votre équipe peut fournir des logiciels plus rapidement et plus efficacement, tout en renforçant la sécurité et la conformité. Nous avons participé à de nombreux événements en 2022, nous en accueillerons et en animerons également en 2023, et nous sommes impatients de vous y rencontrer !"
        link: /events/
        button_text: En savoir plus
        image: /nuxt-images/events/google-cloud-next/pubsec-event-link.jpeg
        alt: une foule de participants lors d'une présentation
        icon: calendar-alt-2
