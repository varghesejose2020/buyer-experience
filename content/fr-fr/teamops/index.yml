---
  title: 'TeamOps : optimisez l''efficacité de vos équipes'
  og_title: 'TeamOps: Optimizing Team Efficiency | GitLab'
  description: TeamOps est une pratique de gestion des équipes axée sur les résultats qui réduit les blocages décisionnels afin d'assurer une mise en œuvre stratégique rapide et efficace. En savoir plus !
  twitter_description: TeamOps est une pratique de gestion des équipes axée sur les résultats qui réduit les blocages décisionnels afin d'assurer une mise en œuvre stratégique rapide et efficace. En savoir plus !
  og_description: TeamOps est une pratique de gestion des équipes axée sur les résultats qui réduit les blocages décisionnels afin d'assurer une mise en œuvre stratégique rapide et efficace. En savoir plus !
  og_image: /nuxt-images/open-graph/teamops-opengraph.png
  twitter_image: /nuxt-images/open-graph/teamops-opengraph.png
  hero:
    logo:
        show: true
    title: |
      Des équipes plus efficaces.
      Des progrès plus rapides.
      Un monde meilleur.
    subtitle: Faire du travail en équipe une collaboration objective
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 200
    image:
      url: /nuxt-images/team-ops/hero-illustration.png
      alt: image hero teamops
      aos_animation: zoom-out-left
      aos_duration: 1600
      aos_offset: 200
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Mobilisez votre équipe
      data_ga_name: enroll
      data_ga_location: hero
  spotlight:
    title: |
      TeamOps accélère la prise de décision
    subtitle: TeamOps est un modèle opérationnel organisationnel qui aide les équipes à optimiser leur productivité, leur flexibilité et leur autonomie en gérant plus efficacement les décisions, les informations et les tâches.
    description:
      "La création d'un environnement propice à prendre de meilleures décisions et à une mieux les appliquer permet de renforcer le travail en équipe et, en fin de compte, de progresser.\n\n\n C'est grâce à TeamOps que GitLab est passée du statut de start-up à une entreprise publique mondiale en l'espace d'une décennie. Aujourd'hui, nous offrons cette possibilité à toutes les organisations."
    list:
      title: Défis courants à relever
      items:
        - Retards dans la prise de décision
        - Lassitude due aux réunions
        - Mauvaise communication en interne
        - Lenteur des transferts et retards dans le déroulement des opérations
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Améliorez l'efficacité de votre équipe
      data_ga_name: make your team better
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  features:
      title: |
        Conçue pour toutes les équipes.
        À distance, hybride ou au bureau.
      image:
        url: /nuxt-images/team-ops/reasons-to-believe.png
        alt: image des raisons de croire en TeamOps
      accordion:
        is_accordion: false
        items:
            - icon:
                name: group
                alt: Icône d'un groupe d'utilisateurs
                variant: marketing
              header: TeamOps est une nouvelle pratique objective de gestion axée sur les résultats
              text: TeamOps aide les organisations à progresser davantage en traitant les relations entre les membres de l'équipe comme un sujet qui peut être opérationnalisé.
            - icon:
                name: clipboard-check-alt
                alt: Icône de presse-papiers Checkmar
                variant: marketing
              header: Un système qui a fait ses preuves
              text: Nous compilons nos logiciels avec TeamOps chez GitLab depuis 5 ans. Notre entreprise est devenue plus productive et les membres de notre équipe montrent une plus grande satisfaction au travail. Cette pratique a connu le jour ici-même, mais nous sommes convaincus qu'elle peut aider la grande majorité des entreprises.
            - icon:
                name: principles
                alt: Icône d'intégration continue
                variant: marketing
              header: Grands principes
              text: TeamOps repose sur quatre principes directeurs qui peuvent aider les entreprises à gérer efficacement les processus dynamiques et évolutifs par nature.
            - icon:
                name: cog-user-alt
                alt: Icône d'un utilisateur dans un rouage
                variant: marketing
              header: Préceptes d'action
              text: Chaque principe est étayé par une série de préceptes d'action, c.-à-d. des méthodes de travail basées sur le comportement, qui peuvent être mises en œuvre immédiatement.
            - icon:
                name: case-study-alt
                alt: Icône d'étude de cas
                variant: marketing
              header: Exemples tirés du monde réel
              text: Nous donnons vie aux préceptes d'action grâce à une bibliothèque croissante d'exemples axés sur les résultats réels de ces préceptes, tels qu'ils sont pratiqués chez Gitlab.
            - icon:
                name: verification
                alt: Icône d'un ruban avec une coche
                variant: marketing
              header: Formation TeamOps
              text: Grâce à la formation TeamOps, nous diffusons cette pratique, afin que les équipes et les entreprises puissent expérimenter son cadre dans un environnement partagé.
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 200
      accordion_aos_animation: fade-left
      accordion_aos_duration: 1600
      aaccordion_os_offset: 200
  video_spotlight:
    title: |
      Plongez dans l'univers TeamOps
    subtitle: Chacun a sa propre opinion lorsqu'il s'agit d'envisager l'avenir du monde du travail.
    description:
      "La formation TeamOps aide les entreprises à réaliser des progrès plus importants en considérant la possibilité d'opérationnaliser les relations entre les membres de l'équipe. En quelques heures seulement, vous pouvez approfondir chacun des principes directeurs et des préceptes d'action de cette pratique, et commencer à évaluer leur compatibilité avec les pratiques opérationnelles actuellement en place au sein de votre équipe."
    video:
      url: 754916142?h=56dd8a7d5d
      alt: image hero teamops
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Mobilisez votre équipe maintenant
      data_ga_name: enroll your team
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  card_section:
    title: |
      Principes directeurs de TeamOps
    subtitle: Les entreprises reposent leur activité sur des personnes et des équipes, ainsi que sur leur créativité, leurs approches et leur humanité.
    cards:
      - title: Une réalité partagée
        ga_name: Shared reality
        description: |
          Alors que d'autres philosophies de gestion donnent la priorité à la vitesse de transfert des connaissances, TeamOps optimise la vitesse de récupération des connaissances.
        icon:
          name: d-and-i
          slp_color: surface-700
        link:
          text: En savoir plus
          url: https://handbook.gitlab.com/teamops/shared-reality/
        color: '#FCA326'
      - title: Tout le monde contribue
        ga_name: Everyone contributes
        description: |
          Les entreprises doivent créer un système dans lequel tout le monde peut consommer de l'information et contribuer, indépendamment de son niveau, de sa fonction ou de sa localisation.
        icon:
          name: user-collaboration
          slp_color: surface-700
        link:
          text: En savoir plus
          url: https://handbook.gitlab.com/teamops/everyone-contributes/
        color: '#966DD9'
      - title: Vélocité de prise de décision
        ga_name: Decision velocity
        description: |
          Tout succès est corrélé à la vélocité de prise de décision, c.-à-d. le nombre de décisions prises dans un laps de temps donné et les résultats qui découlent de cette avancement plus rapide.
        icon:
          name: speed-alt-2
          slp_color: surface-700
        link:
          text: En savoir plus
          url: https://handbook.gitlab.com/teamops/decision-velocity/
        color: '#FD8249'
      - title: Clarté des mesures
        ga_name: Measurement clarity
        description: |
          Il s'agit de mesurer les éléments pertinents. Les principes de prise de décision de TeamOps ne sont utiles que si vous exécutez les processus et en mesurez les résultats.
        icon:
          name: target
          slp_color: surface-700
        link:
          text: En savoir plus
          url: https://handbook.gitlab.com/teamops/measurement-clarity/
        color: '#256AD1'
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  join_us:
    title: |
      Rejoignez le mouvement
    description:
      "TeamOps est un modèle opérationnel organisationnel qui aide les équipes à optimiser leur productivité, leur flexibilité et leur autonomie en gérant plus efficacement les décisions, les informations et les tâches. Rejoignez la liste croissante des organisations qui mettent en pratique TeamOps."
    list:
      title: Principaux défis à relever
      items:
        - Les flux de travail ad hoc empêchent tout alignement des objectifs
        - La débrouille engendre des dysfonctionnements
        - L'infrastructure de communication est une réflexion a posteriori
        - L'obsession du consensus nuit à l'innovation
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Mobilisez votre équipe maintenant
      data_ga_name: enroll
      data_ga_location: join the movement
    quotes:
      - text: Les tests de pré-déploiement ont permis de confirmer que le produit était prêt à être publié ; la fréquence de livraison a également été augmenté.
        author: John Lastname
        note: Director of Job Title, Company name
      - text: Les tests de pré-déploiement ont permis de confirmer que le produit était prêt à être publié ; la fréquence de livraison a également été augmenté.
        author: John Lastname
        note: Director of Job Title, Company name
    clients:
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Logo Cloud Native
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Logo Cloud Native
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Logo Goldman Sachs
      - logo: /nuxt-images/home/logo_siemens_mono.svg
        alt: Logo Siemens
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Logo Cloud Native
      - logo: /nuxt-images/team-ops/logo_knowbe4_mono.svg
        alt: Logo KnowBe4
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Logo Cloud Native
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Logo Cloud Native
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Logo Goldman Sachs
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
