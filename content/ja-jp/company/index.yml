---
  title: GitLabについて
  description: GitLabについて、そしてGitLabの魅力についてご紹介します。
  components:
    - name: 'solutions-hero'
      data:
        title: GitLabについて
        subtitle: DevSecOpsプラットフォームの裏側
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        image:
          image_url: /nuxt-images/company/company_hero.png
          image_url_mobile: /nuxt-images/company/company_hero.png
          alt: "GitLabチームメンバー"
          bordered: true
          rectangular: true
    - name: 'copy-about'
      data:
        title: "当社の事業"
        subtitle: "当社は、最も包括的なDevSecOpsプラットフォームであるGitLabの提供会社です。"
        description: |
          1つのプログラマーチームのコラボレーションを支援するオープンソースプロジェクトとして2011年に誕生し、現在では、セキュリティとコンプライアンスを強化しながら、より速く、より効率的にソフトウェアを提供するために何百万人もの人々に使用されるプラットフォームへと成長しました。
          \
          当初より、リモートワーク、オープンソース、DevSecOps、イテレーションの確固たる信念を貫いてきました。朝起きて（または任意の勤務時間に）ログオンし、チームがツールチェーンではなく、優れたコードをより速くリリースすることに集中できるよう、GitLabコミュニティと協力して毎月新しいイノベーションを届けています。
        cta_text: 'GitLabの詳細を見る'
        cta_link: '/why-gitlab/'
        data_ga_name: "about gitlab"
        data_ga_location: "body"
    - name: 'copy-numbers'
      data:
        title: 数値で知るGitLab
        rows:
          - item:
            - col: 4
              title: |
                **コード**
                **貢献者**
              number: "3,300人以上"
              link: http://contributors.gitlab.com/
              data_ga_name: contributors
              data_ga_location: body
            - col: 4
              title: |
                **オフィス数**
                （創業当初から完全リモート）
              number: "0"
          - item:
            - col: 4
              title: |
                **連続リリース**
                （毎月22日）
              number: "133回"
              link: /releases/
              data_ga_name: releases
              data_ga_location: body
            - col: 4
              title: |
                **チームメンバー**
                （60か国以上）
              number: "1,800人以上"
              link: /company/team/
              data_ga_name: team
              data_ga_location: body
          - item:
            - col: 8
              title: '**推定登録ユーザー数**'
              number: "3,000万人以上"
        customer_logos_block:
          showcased_enterprises:
            - image_url: "/nuxt-images/home/logo_tmobile_white.svg"
              link_label: T-MobileとGitLabのウェブキャストランディングページへのリンク
              alt: "T-Mobileのロゴ"
              url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
            - image_url: "/nuxt-images/home/logo_goldman_sachs_white.svg"
              link_label: ゴールドマン・サックスの顧客事例へのリンク
              alt: "ゴールドマン・サックスのロゴ"
              url: "/customers/goldman-sachs/"
            - image_url: "/nuxt-images/home/logo_cncf_white.svg"
              link_label: Cloud Native Computing Foundationの顧客事例へのリンク
              alt: "クラウドネイティブのロゴ"
              url: /customers/cncf/
            - image_url: "/nuxt-images/home/logo_siemens_white.svg"
              link_label: シーメンスの顧客事例へのリンク
              alt: "シーメンスのロゴ"
              url: /customers/siemens/
            - image_url: "/nuxt-images/home/logo_nvidia_white.svg"
              link_label: NVIDIAの顧客事例へのリンク
              alt: "NVIDIAのロゴ"
              url: /customers/nvidia/
            - image_url: "/nuxt-images/home/logo_ubs_white.svg"
              link_label: ブログ投稿「UBSによる独自のDevOpsプラットフォームを作成するためのGitLab活用方法」へのリンク
              alt: "UBSのロゴ"
              url: /blog/2021/08/04/ubs-gitlab-devops-platform/
        cta_title: |
          計画から生産に至るまで、
          単一のアプリケーションでチーム間の足並みを揃えます
        cta_text: "無料トライアルを開始"
        cta_link: "https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
        data_ga_name: "free trial"
        data_ga_location: "body"
    - name: 'copy-mission'
      data:
        title: "GitLabのミッション"
        description: "当社のミッションは、**誰もが貢献できる**プラットフォームを提供することです。誰もが貢献できるようになれば、ユーザーが貢献者となり、イノベーションの速度を大幅に向上させます。"
        cta_text: '詳しく見る'
        cta_link: '/company/mission/'
        data_ga_name: "mission"
        data_ga_location: "body"
    - name: 'showcase'
      data:
        title: GitLabの価値観
        items:
          - title: コラボレーション
            icon:
              name: collaboration-alt-4
              alt: コラボレーションのアイコン
              variant: marketing
            text: "当社では、前向きな意図を想定する姿勢、感謝やお詫びの言葉、適時のフィードバック共有など、効果的なコラボレーションを実現する上で役立つものを大切にしています。"
            link:
              href: /handbook/values/#collaboration
              data_ga_name: Collaboration
              data_ga_location: body
          - title: 結果
            icon:
              name: increase
              alt: 増加アイコン
              variant: marketing
            text: "当社は迅速かつ行動重視の意識で運営しており、お互いに、お客様に、ユーザーに、そして投資家に対して約束したことを守ります。"
            link:
              href: /handbook/values/#results
              data_ga_name: results
              data_ga_location: body
          - title: 効率
            icon:
              name: digital-transformation
              alt: 効率アイコン
              variant: marketing
            text: "退屈なソリューションを選ぶことから、すべてを文書化し、自己管理することまで、私たちは正しいことに対して迅速な進歩を目指して努力しています。"
            link:
              href: /handbook/values/#efficiency
              data_ga_name: efficiency
              data_ga_location: body
          - title: "多様性、インクルージョン、つながり"
            icon:
              name: community
              alt: コミュニティアイコン
              variant: marketing
            text: "私たちは、GitLabがあらゆる背景や境遇の人たちが一員であると感じ、成長できる場所であるように努めています。"
            link:
              href: "/handbook/values/#diversity-inclusion"
              data_ga_name: diversity inclusion
              data_ga_location: body
          - title: 反復
            icon:
              name: continuous-delivery
              alt: 継続的デリバリーアイコン
              variant: marketing
            text: "私たちは、実行可能で価値のある最小限のことを行い、フィードバックを得るために迅速に提供することを目指しています。"
            link:
              href: "/handbook/values/#iteration"
              data_ga_name: iteration
              data_ga_location: body
          - title: 透明性
            icon:
              name: open-book
              alt: ブックアイコン
              variant: marketing
            text: "会社のハンドブックから製品のイシュートラッカーまで、当社が行うことはすべてデフォルトで公開されています。"
            link:
              href: "/handbook/values/#transparency"
              data_ga_name: transparency
              data_ga_location: body
    - name: 'timeline'
      data:
        title: "GitLabの歩み"
        items:
          - year: "2011"
            description: "GitLabプロジェクトは1つのコミットから始まりました"
          - year: ""
            description: "毎月22日に新しいバージョンのGitLabのリリースを開始しました"
          - year: "2012"
            description: "GitLab CIの最初のバージョンが作成されました"
          - year: "2014"
            description: "GitLabが法人化されました"
          - year: "2015"
            description: "Y Combinatorに参加し、GitLabハンドブックをウェブサイトリポジトリに公開しました"
          - year: "2016"
            description: "マスタープランを発表し、Bラウンド資金調達で2,000万ドルを調達しました"
          - year: "2021"
            description: "GitLab Inc.がNASDAQ Global Market (NASDAQ: GTLB)の上場企業となりました"
    - name: 'copy-about'
      data:
        title: "GitLabで働く"
        description: |
          私たちは、世界中のチームメンバー全員が自分らしく参加して最善の貢献ができ、自分たちの意見が尊重され、また歓迎される、そんな仕事と生活のバランスを真に優先した完全リモートの環境を作り出すことを目指しています。
          \
          私たちのチームに参加してみたいと思った方は、[GitLabでの働き方の詳細](/ jobs/)をご覧になり、自分に向いていると思われた求人に応募してください。
        cta_text: '募集中の職種をすべて表示'
        cta_link: '/jobs/all-jobs/'
        data_ga_name: "all jobs"
        data_ga_location: "body"
    - name: 'teamops'
      data:
        image: "/nuxt-images/company/company-teamops.svg"
        image_alt: "ドキュメントを共有する同僚たち"
        mobile_img: "/nuxt-images/company/TeamOps-mobile.svg"
        header_img: "/nuxt-images/company/TeamOps.svg"
        title: "優れたチーム、早い進展が世界を変える。"
        description: "TeamOpsは、客観的学問としてチームワークを作り上げるGitLab独自の人材育成手法です。GitLabがスタートアップから10年で世界的な上場企業に成長した秘訣です。無料でアクセス可能なプラクティショナー認定を通じて、他の組織がTeamOpsを活かしてより良い意思決定を行い、結果を出し、世界を前進させることができます。"
        button:
            link: "/teamops/"
            text: "TeamOpsの詳細はこちら"
            data_ga_name: "learn more about teamops"
            data_ga_location: "body"
        link:
            link: "https://levelup.gitlab.com/learn/course/teamops"
            text: "認定を受ける"
            data_ga_name: "get certified"
            data_ga_location: "body"
    - name: 'learn-more-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
              hex_color: "#171321"
            event_type: "ブログ"
            header: "GitLab、DevOps、セキュリティなどの最新情報をご覧ください。"
            link_text: "ブログを見る"
            href: "/blog/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLabロゴ
            data_ga_name: "blog"
            data_ga_location: "body"
          - icon:
              name: announcement
              variant: marketing
              alt: Announcement Icon
              hex_color: "#171321"
            event_type: "プレスルーム"
            header: "最近のニュース、プレスリリース、プレスキット。"
            link_text: "もっと詳しく"
            href: "/press/press-kit/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLabロゴ
            data_ga_name: "press kit"
            data_ga_location: "body"
          - icon:
              name: money
              variant: marketing
              alt: Money Icon
              hex_color: "#171321"
            event_type: "投資家情報"
            header: "投資家向けの最新の株式や財務の情報。"
            link_text: "詳しくはこちら"
            href: "https://ir.gitlab.com/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLabロゴ
            data_ga_name: "investor relations"
            data_ga_location: "body"

