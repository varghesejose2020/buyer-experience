---
  title: プラットフォーム
  description: GitLabプラットフォームにより、どのようにチームの連携強化やソフトウェア構築の高速化が実現できるのか、詳細をご覧ください。
  hero: 
    note: 最も包括的な
    header: AIを活用した
    sub_header: DevSecOpsプラットフォーム
    description: 単一の統合開発プラットフォームで、高品質なソフトウェアの迅速なデリバリーを実現します。
    image: 
      src: /nuxt-images/platform/loop-sheild-duo.svg
      alt: 無限シンボル上にDevOpsライフサイクルの計画、コード、ビルド、テスト、リリース、デプロイ、操作、監視が配置され、セキュリティシールドが重なっています。
    button:
      text: 無料トライアルを開始
      href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: 価格を確認する
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero

  features:
    title: チームの最高のパフォーマンスを引き出す 一元化されたプラットフォーム
    subtitle: 1つのアプリケーションでソフトウェアライフサイクルの全ステージをカバー。DevSecOpsをシンプルに実現できます。
    view_all_link:
      view_all_text: ソリューションを確認する
      view_all_url: /solutions/
      data_ga_name: solutions
      data_ga_location: body
    cards: 
      - name: 人工知能と機械学習
        description: 既存のソフトウェアデリバリーの効率を高め、生産性を向上させます。
        icon: ai-summarize-mr-review
        href: /gitlab-duo/
        data_ga_name: GitLab Duo
        data_ga_location: features cards
      - name: ソフトウェアサプライチェーンの安全性
        description: 開発プロセス全体でソフトウェア部品表(SBOM)を作成し、ライセンスコンプライアンスを確認し、セキュリティを自動化します。これにより、リスクを軽減しプロジェクトの遅延を防ぎます。
        icon: shield-check-large
        href: /solutions/dev-sec-ops/
        data_ga_name: DevSecOps
        data_ga_location: features cards
      - name: バリューストリーム管理
        description: 組織内のすべてのステークホルダーに実行可能な洞察を提供し、アイデアの概念的なレベルからその後の開発段階まで、すべてのステージを見える化します。
        icon: visibility
        href: /solutions/value-stream-management/
        data_ga_name: value stream management
        data_ga_location: features cards
      - name: ソースコード管理
        description: GitLabの優れたバージョン管理機能により、開発チームの共同作業を可能にします。そして、生産性の向上、迅速なデリバリー、可視性の向上を実現できます。
        icon: cog-code
        href: /stages-devops-lifecycle/source-code-management/
        data_ga_name: source code management
        data_ga_location: features cards
      - name: 継続的インテグレーションと継続的デリバリー(CI/CD)
        description: コードのビルド、テスト、本番環境へのデプロイを自動化します。
        icon: continuous-integration
        href: /features/continuous-integration/
        data_ga_name: continuous integration
        data_ga_location: features cards
      - name: GitOps
        description: クラウドネイティブ、マルチクラウド、レガシー環境におけるインフラストラクチャの自動化とコラボレーションを実現します。
        icon: digital-transformation
        href: /solutions/gitops/
        data_ga_name: digital transformation
        data_ga_location: features cards
      - name: アジャイルプロジェクトとポートフォリオ管理
        description: 革新的なプロジェクトの計画と管理をサポートし、組織内での作業内容や進捗状況を可視化します。
        icon: agile
        href: /solutions/agile-delivery/
        data_ga_name: agile delivery
        data_ga_location: features cards
    double_card:
      name: チームに最適な料金プランを見つけましょう
      icon: pricing-plans
      cta_one_text: GitLab Ultimate
      cta_one_link: /pricing/ultimate/
      data_ga_name_one: ultimate
      data_ga_location_one: features cards
      cta_two_text: GitLab Premium
      cta_two_link: /pricing/premium/
      data_ga_name_two: premium
      data_ga_location_two: features cards

  table_footer_text: 機能の詳細について学ぶ
  table_footer_link: /features/
  table_footer_data_ga_name: features
  table_footer_data_ga_location: table

  study_card:
    blurb: Forresterの調査
    header: GitLabにより427%のROIを実現
    stats:
      - value: 12倍
        text: 年間リリース数が増加
      - value: 6
        text: か月以下の回収期間
      - value: 87%
        text: 開発とデリバリーの効率を改善
    button: 
      href: https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html
      text: 事例を読む
      data_ga_name: forrester study
      data_ga_location: body

  benefits:
    title: 一元化されたプラットフォームにより
    subtitle: 開発者、セキュリティ担当者、運用チームをシームレスに統合
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: コードソースイメージ
    is_accordion: true
    cta:
      icon: time-is-money
      title: ツールチェーンにどれだけコストをかけていますか？
      text: ROI計算ツールを試す
      link: /calculator/roi/
      data_ga_name: roi calculator
      data_ga_location: body
    items:
      - header: 最高水準のDevSecOps
        text: 
          プラットフォーム全体で統合されたセキュリティを備えた、オールインワンのDevSecOpsソリューション 
          

          単一のデータモデルにより、DevSecOpsライフサイクル全体にわたって総合的な洞察や理解を得られます
      - header: AI駆動のワークフロー革命
        text: 
          計画やコード作成からテスト、セキュリティ、監視まで、ソフトウェア開発ライフサイクルのすべての段階で、AIを駆動。全ユーザーの効率を高め、サイクルタイムを短縮
      - header: DORA指標のデプロイ頻度
        text:
          GitLabをクラウド上で提供されるサービスとして利用予定のお客様向けのSaaS版  
          

          デプロイを自己管理したいお客様向けのSelf-Managed版
          

          コンプライアンスや規制への対応をお望みのお客様向けの、シングルテナントSaaSソリューションであるGitLab Dedicated版
      - header: どんなクラウド環境でも
        text:
          どんなクラウド環境でもデプロイでき、マルチクラウド戦略を可能に 
          

          ベンダーロックインから解放 — あらゆるクラウドプロバイダーに対して同等なサポートを提供
      - header: 一体型ユーザーエクスペリエンス
        text:
          統合された単一プラットフォームにより、異なるツールやアプリケーション間で必要なコンテキストの切り替えを減少
          

          ソフトウェアデリバリーのライフサイクル全体を網羅した1つのプラットフォームを使うことで、新メンバーも素早く効果的に活動を開始でき、トレーニングコストが継続的に削減可能
      - header: 中核部分をコミュニティに開放
        text:
          オープンコア開発モデルにより誰もがコントリビュートでき、ソフトウェアの進化と成長が促進
          

          顧客やコミュニティとともに作り上げることで、より迅速なイテレーションを実現
      - header: シンプルな価格構成
        text:
          アドオン機能は追加料金なしの固定価格

  quotes_carousel_block:
    controls:
      prev: 前の事例
      next: 次の事例
    quotes:
      - main_video: https://www.youtube.com/embed/sT85nT9X2mM
        logo:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: 'Nasdaq'
        url: customers/nasdaq/
        header: <a href='customers/nasdaq/' data-ga-name='nasdaq' data-ga-location='case studies'>Nasdaq社 </a>が GitLab Ultimateで達成したクラウドトランスフォーメーション
        data_ga_name: Nasdaq
        data_ga_location: case studies
        cta_text: この事例に関する動画を見る
        modal: true
        url: https://www.youtube.com/embed/sT85nT9X2mM
        company: Nasdaq
      - quote: "GitLabはセキュリティからパフォーマンス、テストなど全てにおいて、私たちの期待に応えます。"
        author: Chintan Parmar氏
        logo:
          url: /nuxt-images/logos/dunelm.svg
          alt: 'dunelmロゴ'
        role: 主席プラットフォームエンジニア
        statistic_samples:
          - data:
              highlight: 275%
              subtitle: デプロイ回数の増加（週ごと）
        url: /customers/dunelm/
        header: セキュリティを強化するために<a href='/customers/dunelm/' data-ga-name='dunelm' data-ga-location='case studies'>Dunelm社</a> がGitLabを選んだ理由
        data_ga_name: dunelm case study
        data_ga_location: case studies
        cta_text: 詳細はこちら
        company: Dunelm
      - logo:
          url: /nuxt-images/logos/deutsche-telekom-logo-01.jpg
          alt: 'Deutsche Telekomロゴ'
        role: ビジネスオーナーIT、CI/CDハブ
        quote: 市場投入までの長期化は、当社とって大きな問題でした。アジャイルとDevOpsの導入前は、リリースサイクルが18ヶ月近くになることがありましたが、今では約3ヶ月と劇的に短縮することができました。
        author: Thorsten Bastian氏
        statistic_samples:
          - data:
              highlight: 6倍
              subtitle: 速く市場へ投入
          - data:
              highlight: 13,000人
              subtitle: GitLabのアクティブユーザー数
        url: /customers/deutsche-telekom/
        header: <a href='/customers/deutsche-telekom/' data-ga-name='deutsche telekom' data-ga-location='case studies'>Deutsche Telekom社</a> はGitLabによりDevSecOpsトランスフォーメーションを牽引
        data_ga_name: deutsche telekom
        data_ga_location: case studies
        cta_text: 詳細はこちら
        company: Deutsche Telekom
      - logo:
          url: /nuxt-images/logos/carfax-logo.png
          alt: 'carfaxのロゴ'
        quote: DevSecOpsにおいて、セキュリティは常に最優先事項です。プロセスの全段階にセキュリティが組み込まれているのでいて、脆弱性を簡単に見落とすことはありません。
        author: Mark Portofe氏
        role: プラットフォームエンジニアリングディレクター
        statistic_samples:
          - data:
              highlight: 20%
              subtitle: デプロイ回数が増加（前年比）
          - data:
              highlight: 30%
              subtitle: の脆弱性がSDLCの初期段階で発見
        url: /customers/carfax/
        header: <a href='/customers/carfax/' data-ga-name='carfax' data-ga-location='case studies'>CARFAX社</a> はGitLabを導入してセキュリティを強化し パイプラインの管理とコストの最適化を実現
        data_ga_name: carfax
        data_ga_location: case studies
        cta_text: 詳細はこちら
        company: CARFAX
      - logo:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: 'iron mountainのロゴ'
        role: エンタープライズテクノロジー担当バイスプレジデント
        quote: GitLabは、スケーラブルなアジャイルフレームワークを実現する基盤とプラットフォームを提供してくれました。これにより、企業のITチームとステークホルダーとの共同作業が可能になりました。
        author: Hayelom Tadesse氏
        statistic_samples:
          - data:
              highlight: $150,000
              subtitle: コスト削減（年額）
          - data:
              highlight: 20時間
              subtitle: オンボーディング時間を短縮(プロジェクトごと)
        url: /customers/iron-mountain/
        header: <a href='/customers/iron-mountain/' data-ga-name='iron mountain' data-ga-location='case studies'>Iron Mountain社 </a> はGitLab Ultimateを駆使し DevOpsの進化を牽引
        data_ga_name: iron mountain
        data_ga_location: case studies
        cta_text: 詳細はこちら
        company: iron mountain

  report_cta:
    layout: "light"
    statistics:
      - number: 50%以上
        text: パフォーマンス向上（フォーチュン100）
      - number: 3,000万人以上
        text: 登録ユーザー数
    title: DevSecOpsプラットフォームの最先端を切り開くGitLab
    reports:
    - description: "GitLabが『Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023』で唯一のリーダーに認定"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      image: /nuxt-images/logos/forrester-logo.svg
      data_ga_name: forrester
      data_ga_location: reports section
    - description: DevOpsプラットフォームに対する『2023 Gartner® Magic Quadrant™』で、GitLabがリーダーに認定
      url: /gartner-magic-quadrant/
      image: /nuxt-images/logos/gartner-logo.svg
      data_ga_name: Gartner
      data_ga_location: reports section
