---
  title: "スモールビジネス向けDevSecOps - コラボレーションを簡単に"
  description: "GitLabのDevSecOpsプラットフォームを使用してソフトウェアデリバリーを加速し、開発コストを削減して、チームコラボレーションを合理化"
  image_title: "/nuxt-images/open-graph/gitlab-smb-opengraph.png"
  canonical_url: "/small-business/"
  side_navigation_links:
    - title: 概要
      href: '#overview'
    - title: 機能
      href: '#capabilities'
    - title: メリット
      href: '#benefits'
    - title: 事例
      href: '#case-studies'
  solutions_hero:
    title: スモールビジネス向けGitLab
    subtitle: 必要なものがすべて組み込まれたDevSecOpsプラットフォームで、チームの結束力を高めます
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: Ultimateを無料で試用
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: join gitlab
      data_ga_location: header
    secondary_btn:
      text: 価格を確認する
      url: /pricing/
      data_ga_name: Learn about pricing
      data_ga_location: header
    image:
      image_url: /nuxt-images/resources/resources_19.jpg
      alt: "ミーティングを上から見た光景"
      rounded: true
  by_industry_intro:
    logos:
      - name: Hotjar
        image: /nuxt-images/logos/hotjar-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/hotjar/
        aria_label: Hotjarお客様事例へのリンク
      - name: Chorus
        image: /nuxt-images/home/logo_chorus_color.svg
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/chorus/
        aria_label: Chorusお客様事例へのリンク
      - name: Anchormen
        image: /nuxt-images/logos/anchormen-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/anchormen/
        aria_label: Anchormenお客様事例へのリンク
      - name: Remote
        image: /nuxt-images/logos/remote-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/remote/
        aria_label: Remoteお客様事例へのリンク
      - name: Glympse
        image: /nuxt-images/logos/glympse-logo-mono.svg
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/glympse/
        aria_label: G Olympseお客様事例へのリンク
      - name: FullSave
        image: /nuxt-images/case-study-logos/fullsave-logo.png
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/fullsave/
        aria_label: FullSaveお客様事例へのリンク
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: スモールビジネスでは多くの課題に取り組む必要があります。
      description: DevSecOpsのソリューションが、解決しようとしている問題以上の大きな問題を引き起こすべきではありません。 異なるツールを個別に導入して構築された脆弱なツールチェーンとは異なり、GitLabは組織全体の迅速な進化とイノベーションを実現します。複雑さとリスクを排除しながら、高品質でセキュアなソフトウェアを迅速に提供します。
  by_solution_benefits:
    title: 大規模なDevSecOps
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    video:
      video_url: "https://player.vimeo.com/video/703370435?h=fc07a70b24&color=7759C2&title=0&byline=0&portrait=0"
    items:
      - icon:
          name: continuous-integration
          alt: 継続的インテグレーションのイメージ
          variant: marketing
          hex_color: "#171321"
        header: さっそく始めましょう
        text: 全てのDevSecOpsプロセスを一括管理し、迅速にスタートできるテンプレートとベストプラクティスが最初から組み込まれています。
      - icon:
          name: continuous-integration
          alt: 継続的インテグレーションのイメージ
          hex_color: "#171321"
          variant: marketing
        header: DevSecOpsを簡素化
        text: チームは価値の提供に注力すべきであり、ツールチェーンの連携に手間取る必要はありません。
      - icon:
          name: auto-scale
          alt: オートスケールアイコン
          hex_color: "#171321"
          variant: marketing
        header: エンタープライズ向けにスケール可能
        text: ビジネスの成長とともに、DevSecOpsプラットフォームもスケールアップしますが、複雑になることはありません。
      - icon:
          name: devsecops
          alt: DevSecOpsアイコン
          hex_color: "#171321"
          variant: marketing
        header: リスクとコストの削減
        text: スピードや費用を犠牲にすることなく、セキュリティとコンプライアンスを自動化して実施します。
  by_industry_solutions_block:
    subtitle: 主要な機能
    sub_description: "DevSecOpsプラットフォームは、最小限の労力で最大限の顧客価値を創造できるよう、エンドツーエンドのサポートを提供します。 主要な機能は以下になります。"
    white_bg: true
    markdown: true
    sub_image: /nuxt-images/small-business/no-image-alternative-export.svg
    alt: 複数の画面のイメージ
    solutions:
      - title: GitLab Free
        description: |
          **自動化されたソフトウェアデリバリー**

          DevSecOpsの基本であるSCM、CI、CD、GitOpsを使いやすい1つのプラットフォームで提供します。


           **基本的なイシュー管理**

          イシュー(別名ストーリー)を作成し割り当てて、進捗状況を追跡します。


          **基本的なセキュリティスキャン**

          静的アプリケーションセキュリティテスト(SAST)およびシークレットを検出します。
        link_text: 詳細はこちら
        link_url: /pricing/
        data_ga_name: gitlab free
        data_ga_location: body
      - title: GitLab Premium
        description: |
          **自動化されたソフトウェアデリバリー**

          DevSecOpsの基本であるSCM、CI、CD、GpitOsを使いやすい1つのプラットフォームで提供し、追加された管理機能も利用可能です。


           **基本的なイシュー管理**

          イシュー(別名ストーリー)とエピックを作成し割り当てて、進捗状況を追跡します。


          **基本的なセキュリティスキャン**

          静的アプリケーションセキュリティテスト(SAST)およびシークレットを検出します。
        link_text: 詳細はこちら
        link_url: /pricing/premium/
        data_ga_name: gitlab premium
        data_ga_location: body
      - title: GitLab Ultimate
        description: |
          **自動化されたソフトウェアデリバリー**

          DevSecOpsの基本であるSCM、CI、CD、およびGitOpsを1つの使いやすいプラットフォームで提供し、包括的な管理機能でスケーリングを支援します。


          **アジャイル計画**

          イシュー、マルチレベルエピック、バーンダウンチャートなどを盛り込んだプロジェクトプランニングが可能です。


          **包括的なセキュリティテスト**

          SAST、シークレット検出、DAST、コンテナ、依存関係、クラスタイメージ、API、ファズテスト、ライセンスコンプライアンスをカバーする包括的なアプリケーションセキュリティテストが利用できます。


          **脆弱性管理**

          脆弱性評価や、トリアージ、修復など、セキュリティとコンプライアンスの問題を、実用可能なダッシュボードで可視化します。


          **ガバナンス**

          コンプライアンスパイプラインでポリシーとセキュリティガードを自動化します。


          **バリューストリーム管理**

          エンドツーエンドのメトリクスでソフトウェアの開発スピードと成果を向上します。
        link_text: 詳細はこちら
        link_url: /pricing/ultimate/
        data_ga_name: gitlab ultimate
        data_ga_location: body
  by_solution_value_prop:
    title: 開発、SEC、運用のための1つのプラットフォーム
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: SCM
        description: バージョン管理、共同作業、基本的なユーザーストーリーの計画を目的としたソースコード管理。
        href: /stages-devops-lifecycle/source-code-management/
        cta: 詳細
        icon:
          name: cog-code
          alt: Cogコードアイコン
          variant: marketing
      - title: CI/CD
        description: Auto DevOpsによる継続的インテグレーションと継続的デリバリー。
        href: /solutions/continuous-integration/
        cta: 詳細
        icon:
          name: continuous-delivery
          alt: 継続的デリバリーアイコン
          variant: marketing
      - title: GitOps
        description: クラウドネイティブ環境の複雑さを抽象化するインフラストラクチャの自動化。
        href: /solutions/devops-platform/
        cta: 詳細
        icon:
          name: automated-code
          alt: 自動コードアイコン
          variant: marketing
      - title: セキュリティ
        description: 包括的なセキュリティスキャンと脆弱性管理がいつでも使用可能。
        href: /solutions/security-compliance/
        cta: 詳細
        icon:
          name: shield-check
          alt: シールドチェックアイコン
          variant: marketing
  by_industry_case_studies:
    title: お客様が実感したGitLabの価値
    link:
      text: すべての事例
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Anchormen
        subtitle: GitLab CI/CDが Anchormen社のイノベーションを推進させた秘訣
        image:
          url: /nuxt-images/blogimages/anchormen.jpg
          alt: 建物の中でたくさんの光がある
        button:
          href: /customers/anchormen/
          text: 詳細はこちら
          data_ga_name: anchormen learn more
          data_ga_location: body
      - title: Glympse
        subtitle: Glympse社が GitLabを導入してジオロケーション共有をスムーズに実現
        image:
          url: /nuxt-images/blogimages/glympse_case_study.jpg
          alt: 街並みの画像
        button:
          href: /customers/glympse/
          text: 詳細はこちら
          data_ga_name: glympse learn more
          data_ga_location: body
      - title: Hotjar
        subtitle: Hotjar社が GitLabを使用してデプロイを1.5倍加速した方法
        image:
          url: /nuxt-images/blogimages/hotjar.jpg
          alt: トンネルの中
        button:
          href: /customers/hotjar/
          text: 詳細はこちら
          data_ga_name: hotjar learn more
          data_ga_location: body
      - title: Nebulaworks
        subtitle: Nebulaworks社が 3つのツールをGitLab1つに置き換え 顧客のスピードと俊敏性を高めた方法
        image:
          url: /nuxt-images/blogimages/nebulaworks.jpg
          alt: 道具のイメージ
        button:
          href: /customers/nebulaworks/
          text: 詳細はこちら
          data_ga_name: nabulaworks learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    title: リソース
    link:
      text: すべてのリソースを表示
    cards:
      - icon:
          name: ebook-alt
          alt: 電子ブックアイコン
          variant: marketing
        event_type: 電子書籍
        header: DevSecOpsをはじめるためのSMBガイド
        link_text: 詳細はこちら
        image: /nuxt-images/blogimages/vlabsdev_coverimage.jpg
        alt: ノートパソコンで作業するイメージ
        href: https://page.gitlab.com/resources-ebook-smb-beginners-guide-devops.html
        aos_animation: fade-up
        aos_duration: 400
      - icon:
          name: blog-alt
          alt: ブログアイコン
          variant: marketing
        event_type: ブログ
        header: 中小企業のためのDevSecOpsプラットフォーム：6つの手法
        link_text: 詳細はこちら
        image: /nuxt-images/blogimages/shahadat-rahman-gnyA8vd3Otc-unsplash.jpg
        alt: コードの切れ端のイメージ
        href: https://about.gitlab.com/blog/2022/04/12/6-ways-smbs-can-leverage-the-power-of-a-devops-platform/
        aos_animation: fade-up
        aos_duration: 600
      - icon:
          name: blog-alt
          alt: ブログアイコン
          variant: marketing
        event_type: Learn
        header: 1つのアプリケーションでSCM、CI、コードレビューを主導する
        link_text: 詳細はこちら
        image: /nuxt-images/blogimages/zoopla_cover_image.jpg
        alt: 近所のイメージ
        href: https://learn.gitlab.com/smb-ci-1/leading-scm-ci-and-c
        aos_animation: fade-up
        aos_duration: 800
      - icon:
          name: video
          alt: ビデオアイコン
          variant: marketing
        event_type: ビデオ
        header: GitLab DevSecOpsプラットフォームのデモを見る
        link_text: デモを視聴する
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        alt: gitlab devopsのイメージ
        href: https://learn.gitlab.com/smb-beginners-devops/oei67xcnxmk
        aos_animation: fade-up
        aos_duration: 1000
      - icon:
          name: case-study
          alt: ケーススタディアイコン
          variant: marketing
        event_type: ブログ
        header: DevOpsプラットフォームには、SMBやスタートアップが小さすぎることはありますか？
        link_text: 詳細はこちら
        image: /nuxt-images/features/resources/resources_case_study.png
        alt: 上から見た木
        href: https://about.gitlab.com/blog/2022/04/06/can-an-smb-or-start-up-be-too-small-for-a-devops-platform/
        aos_animation: fade-up
        aos_duration: 1200
