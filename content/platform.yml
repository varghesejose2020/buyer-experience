---
  title: Platform
  description: Learn more about how the GitLab platform can help teams collaborate and build software faster.
  hero: 
    note: The most comprehensive 
    header: AI-powered
    sub_header: DevSecOps Platform
    description: Deliver better software faster with one platform for your entire software delivery lifecycle
    image: 
      src: /nuxt-images/platform/loop-sheild-duo.svg
      alt: The DevOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield.
    button:
      text: Get Free Trial
      href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: Learn about pricing
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero

  features:
    title: Teams can do more with a single platform
    subtitle: DevSecOps simplified through a single integrated application for use across teams and software lifecycle stages
    view_all_link:
      view_all_text: Explore solutions
      view_all_url: /solutions/
      data_ga_name: solutions
      data_ga_location: body
    cards: 
      - name: Artificial intelligence and machine learning
        description: Make existing software delivery workflows more intelligent, improving productivity and efficiency.
        icon: ai-summarize-mr-review
        href: /gitlab-duo/
        data_ga_name: gitlab duo
        data_ga_location: features cards
      - name: Software supply chain security
        description: Create software bill of materials (SBOM), check license compliance, and automate security throughout the development process to reduce risk and delays.
        icon: shield-check-large
        href: /solutions/dev-sec-ops/
        data_ga_name: devsecops
        data_ga_location: features cards
      - name: Value stream management
        description: Provide actionable insight to every stakeholder in the organization, with visibility into every stage of ideation and development.
        icon: visibility
        href: /solutions/value-stream-management/
        data_ga_name: value stream management
        data_ga_location: features cards
      - name: Source code management
        description: Version control to help your development team collaborate and maximize productivity, sparking faster delivery and increased visibility.
        icon: cog-code
        href: /stages-devops-lifecycle/source-code-management/
        data_ga_name: source code management
        data_ga_location: features cards
      - name: Continuous integration and continuous delivery (CI/CD)
        description: Automation to build, test, and deploy your code to your production environment.
        icon: continuous-integration
        href: /features/continuous-integration/
        data_ga_name: continuous integration
        data_ga_location: features cards
      - name: GitOps
        description: Infrastructure automation and collaboration for cloud native, multi-cloud, and legacy environments.
        icon: digital-transformation
        href: /solutions/gitops/
        data_ga_name: digital transformation
        data_ga_location: features cards
      - name: Agile project and portfolio management
        description: Plan, initiate, prioritize, and manage innovation initiatives, with complete visibility into the work your teams are doing.
        icon: agile
        href: /solutions/agile-delivery/
        data_ga_name: agile delivery
        data_ga_location: features cards
    double_card:
      name: Find out which pricing plan is best for your team
      icon: pricing-plans
      cta_one_text: GitLab Ultimate
      cta_one_link: /pricing/ultimate/
      data_ga_name_one: ultimate
      data_ga_location_one: features cards
      cta_two_text: GitLab Premium
      cta_two_link: /pricing/premium/
      data_ga_name_two: premium
      data_ga_location_two: features cards

  table_footer_text: Learn about our Features
  table_footer_link: /features/
  table_footer_data_ga_name: features
  table_footer_data_ga_location: table

  study_card:
    blurb: Forrester study
    header: GitLab enabled 427% ROI
    stats:
      - value: 12x
        text: increase in number of annual releases
      - value: <6
        text: months payback period
      - value: 87%
        text: improved development and delivery efficiency
    button: 
      href: https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html
      text: Read the study
      data_ga_name: forrester study
      data_ga_location: body

  benefits:
    title: One platform
    subtitle: to empower dev, sec, and ops teams
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: code source image
    is_accordion: true
    cta:
      icon: time-is-money
      title: How much is your current toolchain costing you?
      text: Find out
      link: /calculator/roi/
      data_ga_name: roi calculator
      data_ga_location: body
    items:
      - header: Best-in-class DevSecOps
        text: 
          An all-in-one DevSecOps solution with security integrated throughout the platform
          

          Single data model allows for insights across your entire DevSecOps lifecycle
      - header: AI-powered workflows
        text: 
          Boost efficiency and reduce cycle times of every user with the help of AI in every phase of the software development lifecycle - from planning and code creation to testing, security, and monitoring
      - header: Deployment flexibility
        text:
          SaaS for customers who want to consume GitLab as a service
          

          Self-managed for customers who want deployment control
          

          GitLab Dedicated, a single tenant SaaS offering, for customers with data isolation and residency needs
      - header: Cloud agnostic
        text:
          Deploy anywhere, enabling multi-cloud strategy
          

          Avoid vendor lock-in — no preferential treatment of any single cloud provider
      - header: Integrated user experience
        text:
          Reduce context switching with an integrated, single-platform experience 
          

          Faster onboarding and reduced ongoing training costs with one platform across the entire software delivery lifecycle
      - header: Open core
        text:
          Rapid innovation since anyone can contribute through our open core development model
          

          Faster iteration as we build with our customers and community
      - header: No surprise costs
        text:
          Fixed pricing with no additional costs for add-on capabilities

  quotes_carousel_block:
    controls:
      prev: previous case study
      next: next case study
    quotes:
      - main_video: https://www.youtube.com/embed/sT85nT9X2mM
        logo:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: 'Nasdaq'
        url: customers/nasdaq/
        header: How <a href='customers/nasdaq/' data-ga-name='nasdaq' data-ga-location='case studies'>Nasdaq</a> is achieving cloud transformation with GitLab Ultimate
        data_ga_name: Nasdaq
        data_ga_location: case studies
        cta_text: Watch the video
        modal: true
        url: https://www.youtube.com/embed/sT85nT9X2mM
        company: Nasdaq
      - quote: "GitLab can do everything we want it to do, from security, performance, testing, and more."
        author: Chintan Parmar
        logo:
          url: /nuxt-images/logos/dunelm.svg
          alt: 'dunelm logo'
        role: Principal Platform Engineer
        statistic_samples:
          - data:
              highlight: 275%
              subtitle: increase in weekly deployments
        url: /customers/dunelm/
        header: Why <a href='/customers/dunelm/' data-ga-name='dunelm' data-ga-location='case studies'>Dunelm</a> chose GitLab to boost security
        data_ga_name: dunelm case study
        data_ga_location: case studies
        cta_text: Read more
        company: Dunelm
      - logo:
          url: /nuxt-images/logos/deutsche-telekom-logo-01.jpg
          alt: 'Deutsche Telekom logo'
        role: Business Owner IT, CI/CD Hub
        quote: Time to market was a big issue for us. Before our transformation to Agile and DevOps started, we had release cycles of nearly 18 months in some cases. We've been able to dramatically reduce that to roughly 3 months.
        author: Thorsten Bastian
        statistic_samples:
          - data:
              highlight: 6x
              subtitle: faster time to market
          - data:
              highlight: 13,000
              subtitle: active GitLab users
        url: /customers/deutsche-telekom/
        header: <a href='/customers/deutsche-telekom/' data-ga-name='deutsche telekom' data-ga-location='case studies'>Deutsche Telekom:</a> Driving DevSecOps transformation with GitLab
        data_ga_name: deutsche telekom
        data_ga_location: case studies
        cta_text: Read more
        company: Deutsche Telekom
      - logo:
          url: /nuxt-images/logos/carfax-logo.png
          alt: 'carfax logo'
        quote: With DevSecOps, security is always front and center. It's part of every step of the process and not easily missed.
        author: Mark Portofe
        role: Director of Platform Engineering
        statistic_samples:
          - data:
              highlight: 20%
              subtitle: boost in deployments YoY
          - data:
              highlight: 30%
              subtitle: of vulnerabilities found earlier in SDLC
        url: /customers/carfax/
        header: <a href='/customers/carfax/' data-ga-name='carfax' data-ga-location='case studies'>CARFAX</a> improves security, cuts pipeline management and costs with GitLab
        data_ga_name: carfax
        data_ga_location: case studies
        cta_text: Read more
        company: CARFAX
      - logo:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: 'iron mountain logo'
        role: Vice President of Enterprise Technology
        quote: GitLab has provided us with the foundation and platform to enable our scaled Agile framework. We are able to collaborate within our Enterprise IT teams and our key stakeholders.
        author: Hayelom Tadesse
        statistic_samples:
          - data:
              highlight: $150k
              subtitle: in approximate cost savings per year
          - data:
              highlight: 20 hrs
              subtitle: saved in onboarding time per project
        url: /customers/iron-mountain/
        header: <a href='/customers/iron-mountain/' data-ga-name='iron mountain' data-ga-location='case studies'>Iron Mountain</a> drives DevOps evolution with GitLab Ultimate
        data_ga_name: iron mountain
        data_ga_location: case studies
        cta_text: Read more
        company: Iron Mountain

  report_cta:
    layout: "light"
    statistics:
      - number: 50%+
        text: Fortune 100
      - number: 30m+
        text: Registered users
    title: GitLab is the leading DevSecOps platform
    reports:
    - description: "GitLab recognized as the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      image: /nuxt-images/logos/forrester-logo.svg
      data_ga_name: forrester
      data_ga_location: reports section 
    - description: GitLab recognized as a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms
      url: /gartner-magic-quadrant/
      image: /nuxt-images/logos/gartner-logo.svg
      data_ga_name: gartner
      data_ga_location: reports section
