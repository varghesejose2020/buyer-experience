title: GitLab’s AI-assisted Code Suggestions
og_title: GitLab’s AI-assisted Code Suggestions
description: GitLab’s AI-assisted Code Suggestions. Keeps your proprietary source code secure. Helps you code more productively. Puts billions of lines of code at your fingertips.
twitter_description: GitLab’s AI-assisted Code Suggestions. Keeps your proprietary source code secure. Helps you code more productively. Puts billions of lines of code at your fingertips.
og_description: GitLab’s AI-assisted Code Suggestions. Keeps your proprietary source code secure. Helps you code more productively. Puts billions of lines of code at your fingertips.
og_image: /nuxt-images/solutions/code-suggestions/secure-code.jpeg
twitter_image: /nuxt-images/solutions/code-suggestions/secure-code.jpeg
hero: 
  note: GitLab Duo Code Suggestions
  header: (beta)
  description: 
    - typed:
        - Helps you stay
      is_description_inline: true
      highlighted: in flow
    - typed:
        - In your IDE 
      highlighted: of choice
      is_description_inline: true
    - typed:
        - In the language
      highlighted: you need
      is_description_inline: true
  image: 
    src: /nuxt-images/solutions/ai/GitLab Logo.png
    alt: AI logo
  button:
    text: Try code suggestions for free
    href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
    variant: secondary
copyBlocks:
  blocks: 
    - title: Accelerate coding
      description: Code Suggestions helps developers stay in flow by predicting and completing blocks of code in the context, defining and generating logic for function declarations, generating tests, and suggesting common code like regex patterns. Code Suggestions helps teams create software faster and more efficiently.
    - title: Does not use your proprietary code as training data
      description: Code Suggestions is built with privacy as a critical foundation. Private, non-public customer code stored in GitLab is not used as training data. [Learn about data usage](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html?_gl=1*3eiksj*_ga*MTEwODM4NzI0OC4xNjgyNzAwNTA1*_ga_ENFH3X7M5Y*MTY4NDM4NDE3My42Ni4xLjE2ODQzODU0NDcuMC4wLjA.#code-suggestions-data-usage){data-ga-name="code suggestions data usage" data-ga-location="body"} when using Code Suggestions.
      img:
        src: /nuxt-images/solutions/code-suggestions/secure-code.svg
        alt: secure code logo

    - title: Support in the language you need
      description: 'AI-powered Code Suggestions that fit the way you work are available in [14 languages](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#supported-languages){data-ga-name="code suggestions supported languages" data-ga-location="body"}: C++, C#, Go, Google SQL, Java, JavaScript, Kotlin, PHP, Python, Ruby, Rust, Scala, Swift, TypeScript.'
      logos: [
        "/nuxt-images/logos/c-logo.svg",
        "/nuxt-images/logos/cpp-logo.svg",
        "/nuxt-images/logos/c-sharp-logo.svg",
        "/nuxt-images/logos/javascript-logo.svg",
        "/nuxt-images/logos/typescript-logo.svg",
        "/nuxt-images/logos/python-logo.svg",
        "/nuxt-images/logos/java-logo.svg",
        "/nuxt-images/logos/ruby-logo.svg",
        "/nuxt-images/logos/rust-logo.svg",
        "/nuxt-images/logos/google-cloud-sql.svg",
        "/nuxt-images/logos/scala-logo.svg",
        "/nuxt-images/logos/kotlin-logo.svg",
        "/nuxt-images/logos/go-logo.svg",
        "/nuxt-images/logos/php-logo.svg",
      ]
    - title: Meets you in your IDE of choice 
      description: 'You can find our GitLab extensions in popular IDE marketplaces. Support includes: GitLab Web IDE, VS Code, Visual Studio, Jetbrains-based IDEs, and NeoVIM. Learn more about [IDE support](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html){data-ga-name="code suggestions IDE support" data-ga-location="body"} for Code Suggestions.'
      languageSelected: 'go'
    - title: Available for self-managed instances 
      description: Code Suggestions is available to self-managed GitLab instances via a secure connection to GitLab.com. [Learn more](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-self-managed-gitlab){data-ga-name="code suggestions self managed" data-ga-location="body"}.
      languageSelected: 'javascript'
ctaBlock:
    title: What’s next for your AI pair programmer?
    cards:
      - header: Improved Suggestion Quality
        description: |
          We’re continuously improving suggestion quality with new prompt engineering, intelligent model routing, and expanded contexts for inference windows, all in the works. [You can follow our progress and even suggest ideas](https://gitlab.com/groups/gitlab-org/-/epics/9814).
        icon: ai-code-suggestions
      - header: General Availability
        description: | 
          We’re actively working on improved stability, performance, availability, and user experience improvements as we approach general availability of Code Suggestions. Learn about [GitLab’s feature support matrix](https://docs.gitlab.com/ee/policy/experiment-beta-support.html){data-ga-name="gitlab support matrix" data-ga-location="body"}.
        icon: user-laptop
resources:
    data:
      title: What's new in for AI-powered Code Suggestions
      column_size: 4
      cards:
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: Code Suggestions powered by Google AI
          link_text: Read more
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          href: https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/#gitlab-duo-code-suggestions-improvements-powered-by-google-ai/
          data_ga_name: Code Suggestions powered by Google AI
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: >-
            Introducing Jetbrains and NeoVim support
          link_text: Read more
          href: https://about.gitlab.com/blog/2023/07/25/gitlab-jetbrains-neovim-plugins/ 
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: Intorducing Jetbrains and NeoVim support
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: Visual studio support
          link_text: Read more
          href: https://about.gitlab.com/blog/2023/06/29/gitlab-visual-studio-extension/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: Visual studio support
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: Self managed support
          link_text: Read more
          href: https://about.gitlab.com/blog/2023/06/15/self-managed-support-for-code-suggestions/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: Self managed support
          data_ga_location: body


