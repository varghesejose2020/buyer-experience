---
  title: GitLab for Nonprofits
  description: Create. Configure. Monitor. Secure. Nonprofits benefit from The DeveSecOps Platform
  next_step_alt: true
  no_gradient: true
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for Nonprofits
        subtitle: Create. Configure. Monitor. Secure. Nonprofits benefit from The DeveSecOps Platform.*
        footnote: " *The GitLab for Nonprofit Program operates on a first come-first served basis each year. Once we reach our donation limit, the application will no longer be available. "
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start yout free trial
          url: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/&glm_content=default-saas-trial
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Join the program
          url: /solutions/nonprofit/join/
          data_ga_name: join nonprofit
          data_ga_location: hero
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/nonprofit-hero.jpeg
          image_url_mobile: /nuxt-images/solutions/nonprofit-hero.jpeg
          alt: ""
          bordered: true
          image_purple_background: true
    - name: 'side-navigation-variant'
      links:
        - title: Overview
          href: '#overview'
        - title: Testimonials
          href: '#testimonials'
        - title: Benefits
          href: '#benefits'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'nonprofit-intro'
              data:
                text:
                  description: GitLab is a single platform for project management, collaboration, source control management, git automation,secuity and much more. Because it is east to use, flexible, and all in one place, it is the best choice for nonprofits to scale their work. The GitLab for Nonprofits Program gives free licenses of GitLab to registered nonprofit organizations.
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'quotes-carousel'
              data:
                case_studies:
                  - logo_url: /nuxt-images/case-study-logos/the_last_mile_logo.png
                    institution_name: Last Mile
                    quote:
                      quote_text: GitLab is our canvas for workflow across our nonprofit organization. It allows us to do everything from plan and manage the workloads for our teams, all the way to the CI/CD pipelines which automate efforts ranging from routine tasks, to platform deployments in our various environments. Whether it's documenting processes in place with issue templates, or structuring organization wide initiatives with interdependent issues, epics, and milestones; GitLab gives us the structure we need, with the flexibility to meet our unique needs.
                      author: Mike Bowie
                      author_title: Chief Technology Officer, Last Mile
        - name: 'div'
          id: benefits
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Why nonprofits choose GitLab
                light_background: true
                large_card_on_bottom: true
                cards:
                  - title: One platform for the entire lifecycle
                    description: With a single application for the entire software delivery lifecycle, your teams can focus on shipping great software — not maintaining numerous toolchain integrations. You’ll improve communication, end-to-end visibility, security, cycle times, onboarding, and much more.
                    href: /platform/?stage=plan
                    data_ga_name: platform plan
                    icon:
                      name: cycle
                      alt: cycle Icon
                      variant: marketing
                  - title: Deploy to any environment or cloud
                    description: GitLab is cloud-neutral, so you have the freedom to use it how and where you want — giving you the flexibility to change and add cloud providers as you grow. Deploy seamlessly to AWS, Google Cloud, Azure, and beyond.
                    icon:
                      name: merge
                      alt: merge Icon
                      variant: marketing
                  - title: Built-in security
                    description: With GitLab, security and compliance are built into every stage of the software delivery lifecycle, enabling your team to identify vulnerabilities earlier and making development faster and more efficient. As your startup grows, you’ll be able to manage risk without sacrificing speed.
                    href: /solutions/security-compliance/
                    data_ga_name: solutions dev-sec-ops
                    icon:
                      name: devsecops
                      alt: devsecops Icon
                      variant: marketing
                  - title: Create and deploy software faster
                    description: GitLab enables you to decrease cycle times and deploy more frequently with less effort. Do everything from planning new features to automated testing and release orchestration, all in a single application.
                    href: /platform/
                    data_ga_name: platform
                    icon:
                      name: speed-gauge
                      alt: speed-gauge Icon
                      variant: marketing
                  - title: Stronger collaboration
                    description: Break down silos and empower everyone in your company to collaborate around your code — from development, security, and operations teams to business teams and non-technical stakeholders. With The DevSecOps Platform, you can increase visibility across the entire lifecycle and help improve communication for everyone working together to move software projects forward.
                    icon:
                      name: collaboration-alt-4
                      alt: Collaboration Icon
                      variant: marketing
                  - title: Open source platform
                    description: GitLab’s open core gives you all the benefits of open source software, including the opportunity  to leverage the innovations of thousands of developers worldwide continuously working to improve and refine it.
                    icon:
                      name: open-source
                      alt: open-source Icon
                      variant: marketing
    - name: single-cta-block
      data:
        cards:
          - header: See if the GitLab for Nonprofits Program is right for you.
            icon: handshake
            link:
              text: Join the program
              url: /solutions/nonprofit/join/
              data_ga_name: startup join