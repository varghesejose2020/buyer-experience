---
  title: SAST vs. DAST
  description: Discover the difference between SAST and DAST. Explore this comprehensive overview to understand how these security testing methods can safeguard your systems. Learn more now!
  date_published: 2023-08-07
  date_modified: 2023-08-07
  topics_header:
    data:
      title: SAST vs. DAST
      block:
        - metadata:
            id_tag: sast-vs-dast
          text: |
            What’s the difference between these popular application security testing tools, and which should your team use? Let’s break it down.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevSecOps
      href: /topics/devsecops/
      data-ga-name: devsecops
      data_ga_location: breadcrumb
    - title: SAST vs. DAST
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: What are SAST and DAST?
          href: "#what-are-sast-and-dast"
          data_ga_name: whare are sast and dast
          data_ga_location: side-navigation
        - text: Why is SAST important?
          href: "#why-is-sast-important"
          data_ga_name: why is sast important
          data_ga_location: side-navigation
        - text: Why is DAST important?
          href: "#why-is-dast-important"
          data_ga_name: why is dast important
          data_ga_location: side-navigation
        - text: How widely used are SAST and DAST?
          href: "#how-widely-used-are-sast-and-dast"
          data_ga_name: how widely used are sast and dast
          data_ga_location: side-navigation
        - text: What SAST and DAST can detect
          href: "#examples-of-what-sast-and-dast-can-detect"
          data_ga_name: examples of what sast and dast can detect
          data_ga_location: side-navigation
        - text: Getting the most out of SAST and DAST
          href: "#getting-the-most-out-of-sast-and-dast"
          data_ga_name: Getting the most out of SAST and DAST
          data_ga_location: side-navigation
        - text: When should you use DAST vs. SAST?
          href: "#when-should-you-use-dast-vs-sast"
          data_ga_name: When should you use DAST vs. SAST?
          data_ga_location: side-navigation
        - text: SAST and DAST main differences
          href: "#what-are-the-main-differences-between-sast-and-dast"
          data_ga_name: What are the main differences between SAST and DAST
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
    content:
      - name: 'topics-copy-block'
        data:
          header: What are SAST and DAST?
          column_size: 10
          blocks:
              - text: |
                  SAST and DAST are two types of application security testing used to detect security vulnerabilities.


                  ### What is SAST?

                  * SAST, which stands for _static_ application security testing, is a type of white-box testing that analyzes _source code_ for known security vulnerabilities. 
                  * SAST runs before code is deployed — and ideally, right after it’s committed — so developers and the rest of the team are alerted to any security issues sooner rather than later and are able to fix them while the code is still being developed.

                  ### What is DAST?

                  * DAST, which stands for _dynamic_ application security testing, is a type of black-box testing that looks for security vulnerabilities that malicious attackers could exploit in your _running web applications_. 
                  * DAST scans the entire application, as well as other APIs and web services your application uses, to check if well-known attack techniques are possible. DAST tools simulate attacks and help you see your web application through the eyes of a hacker in a deployed environment.

      - name: 'topics-copy-block'
        data:
          header: Why is SAST important?
          column_size: 10
          blocks:
            - text: |
                SAST is an important way to catch security vulnerabilities early on while the code is still being developed, and long before it’s been deployed. Catching vulnerabilities earlier in the development process typically makes them cheaper and easier to fix.
      - name: 'topics-copy-block'
        data:
            header: Why is DAST important?
            column_size: 10
            blocks:
              - text: |
                  DAST helps you catch security issues and vulnerabilities in your applications that are unlikely to be caught by other traditional testing methods that focus on the code and technology within your application. DAST simulates attacks on the application to identify security weaknesses where an attacker could get in, so you can fix them before they can be exploited by real attackers.
      - name: 'topics-copy-block'
        data:
            header: How widely used are SAST and DAST?
            column_size: 10
            blocks:
              - text: |
                  SAST and DAST are becoming increasingly common tools for DevOps teams. 

                  According to GitLab’s [2022 Global DevSecOps Survey](https://about.gitlab.com/developer-survey/), 53% of developers now run SAST scans (up from less than 40% in 2021) and 55% of developers run DAST scans (up from 44% in 2021).
      - name: 'topics-copy-block'
        data:
          header: Examples of what SAST and DAST can detect
          column_size: 10
          blocks:
            - text: |
                SAST and DAST largely detect different types of vulnerabilities and security issues. Here are some of the security issues SAST and DAST can identify.

                SAST can detect:
                * SQL injection
                * Buffer overflows
                * XML External Entity (XXE) vulnerabilities
                * Critical security vulnerabilities identified in industry standards like OWASP Top 10 and SANS/CWE Top 25

                DAST can detect:
                * Cross-site scripting (XXS)
                * SQL injection
                * Broken authentication flaws
                * Encryption issues
                * Misconfigurations of your application server or databases
                * Incorrect assumptions about security controls that may not be visible from the source code
      - name: 'topics-copy-block'
        data:
          header: Getting the most out of SAST and DAST
          column_size: 10
          blocks:
            - text: |
                To get the full benefits of SAST and DAST, it’s best to:
                * Build/Integrate SAST and DAST into your team’s workflow and CI/CD pipeline.
                * Make SAST and DAST auto-run so your team doesn’t have to manually initiate the tests.
                * Ensure running SAST and DAST can’t be bypassed or forgotten.
                * Put restrictions in place so that code with detected vulnerabilities cannot be merged without the proper approvals.
                * Make sure the SAST and DAST analyzers you use are updated regularly so you benefit from the latest vulnerabilities definitions.
                * Use SAST and DAST in a way that all of your teams — development, ops and security — can easily see the results of scans and collaborate to fix security issues.
      - name: 'topics-copy-block'
        data:
          header: When should you use DAST vs. SAST?
          column_size: 10
          blocks:
            - text: |
                You’ll want to use _both_ SAST and DAST to help your team ship secure software and catch security issues earlier when they’re less likely to slow you down.

                SAST should be run very early in the software development lifecycle, ideally as soon as code is committed. This means that security vulnerabilities in the code can be detected and highlighted to the person who committed the code while it’s still fresh in their mind.

                DAST should be run anytime you make a change to your application, ideally when it’s deployed in a test environment so you can catch issues before they make it to production. DAST can also be used to continuously monitor live web applications for issues like cross-site scripting or broken authentication flaws.
      - name: 'topics-copy-block'
        data:
          header: What are the main differences between SAST and DAST?
          column_size: 10
          blocks:
            - text: | 
                **What they scan**

                SAST scans source code, while DAST scans applications and APIs or web services your application connects to, such as GraphQL, REST, and SOAP.

                **When they scan**

                SAST happens early in the software development lifecycle shortly after code is written, while DAST happens later in the development lifecycle once there’s a working application running in a test environment, or even on production code.

                **Difference in the types of testing**

                SAST is [white-box testing](https://en.wikipedia.org/wiki/White-box_testing) that looks for vulnerabilities inside the application and code, while DAST is [black-box testing](https://en.wikipedia.org/wiki/Black-box_testing) that looks for vulnerabilities that could allow an outside attacker to get in.

                **Having access to course code**

                SAST tools scan the source code of an application, while DAST tools do not have access to source code.

                **Difference in language dependence**

                Because SAST is scanning your source code, it’s specific to the programming languages and development frameworks used, and the SAST tool you use needs to support the programming language you are using — whether it’s C++, Python, Go, React, Ruby, or something else. 

                Unlike SAST, DAST doesn’t care what languages or frameworks your application is built on because it’s testing your application from the outside like an attacker would.

                **False positives**

                SAST tends to produce more false positives than DAST. This is because it’s focused on source code and doesn’t have all the context to know if one line of code that looks problematic is actually solved somewhere else. Some DAST providers, such as GitLab, are able to identify some false positives in SAST. 
      - name: 'topics-copy-block'
        data:
          header: SAST and DAST with GitLab
          column_size: 10
          blocks:
            - text: |
                GitLab’s DevSecOps platform can help you get the most out of SAST and DAST — and much more — to help you improve the security of your applications without sacrificing speed.

                Learn more:
                * [Our documentation](https://docs.gitlab.com/ee/user/application_security/secure_your_application.html)
                * [Our solutions page on DevSecOps, security, and governance](https://about.gitlab.com/solutions/dev-sec-ops/)