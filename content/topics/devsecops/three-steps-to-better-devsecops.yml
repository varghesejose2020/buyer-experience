---
  title: Three DevSecOps best practices to implement immediately
  description: With bad actors targeting code more than ever, it's time to embrace the importance of DevSecOps. Here are three steps teams can take now to improve their application security.
  topics_header:
    data:
      title: Three DevSecOps best practices to implement immediately

      block:
        - metadata:
            id_tag: three-steps-to-better-devsecops
          text: |
            With bad actors targeting code more than ever, it's time to embrace the importance of DevSecOps. Here are three steps teams can take now to improve their application security.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevSecOps
      href: /topics/devsecops/
      data-ga-name: devsecops
      data_ga_location: breadcrumb
    - title: Three DevSecOps best practices to implement immediately
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: 1. Start with collaboration and communication
          href: "#1-start-with-collaboration-and-communication"
          data_ga_name: 1. Start with collaboration and communication
          data_ga_location: side-navigation
        - text: 2. Write once, test often
          href: "#2-write-once-test-often"
          data_ga_name: 2. Write once, test often
          data_ga_location: side-navigation
        - text: 3. Use test results to educate, not punish
          href: "#3-use-test-results-to-educate-not-punish"
          data_ga_name: 3. Use test results to educate, not punish
          data_ga_location: side-navigation
        - text: "Learn more about DevSecOps:"
          href: "#learn-more-about-dev-sec-ops"
          data_ga_name: Learn more about DevSecOps
          data_ga_location: side-navigation
    hyperlinks:
      text: ""
      data: []
    content:
      - name: topics-copy-block
        data:
          no_header: true
          column_size: 10
          blocks:
            - text: |
                No matter where you are on your [DevOps journey](/topics/devops/){data-ga-name="devops journey" data-ga-location="body"}, it's time to lean into [DevSecOps](/topics/devsecops/){data-ga-name="devsecops" data-ga-location="body"} like never before. Why? Because the at-risk surfaces have never been larger. Today, applications are the single biggest security target, according to Forrester Research VP Amy DeMartine, who stressed [the problem is getting worse and not better](/blog/2020/10/14/why-security-champions/){data-ga-name="worse not better" data-ga-location="body"}.

                Complicating matters further, research from Gartner shows [IT spending on security](https://www.cybersecuritydive.com/news/security-budget-gartner/587911/) is actually falling and represented just 5.7% of the total budget in 2019.
                And as if all that is not enough, there is ongoing confusion within DevOps teams about exactly which groups “own” the responsibility for security. In our 2020 Global DevSecOps Survey, 33% of security pros said they were solely responsible for security, but nearly as many - 29% - said everyone was.

                It's time to rethink [how teams approach DevSecOps](/solutions/security-compliance/){data-ga-name="approach devsecops" data-ga-location="body"}, starting from the ground up. Here are three strategies teams can implement immediately.
      - name: topics-copy-block
        data:
          header: 1. Start with collaboration and communication
          column_size: 10
          blocks:
            - text: |
                Collaboration is the key to success for every project. Bring together your project leaders and security delegates. Give them one meeting (with a pre-read or pre-plan) to devise a set of security measures that need to be fulfilled by code written for this project, and plan out the automated tests that developers will need to run on their code. Making these decisions together will foster both trust in the process and encourage buy-in to a security-by-design mentality.

                [Rob Cuddy from IBM](https://securityintelligence.com/take-your-relationship-with-devsecops-to-the-next-level/) advises your joint team adopt three important communication points to take your DevSecOps to the next level:
                Communicate only serious issues - and filter out excess noise using AI and machine learning to verify your security scans.
                Talk about the elephant in the room: Open source. Third party and open source code is omnipresent in software development, so it's critical to address it directly to reduce the likelihood of preventable attacks.

                Get to the root issues and deal with them faster: Find and fix false negatives before they're exploited.

                Take these steps to encourage direct, honest, and tactful communication across teams - it will help you build and maintain a level of trust and credibility critical to efficient and effective DevSecOps.


      - name: topics-copy-block
        data:
          header: 2. Write once, test often
          column_size: 10
          blocks:
            - text: |
                Given the limited resources available for application security and its important role in business success, it only makes sense to run tests at every code commit. Ideally, these tests will be written once to meet project or organizational standards, and then run automatically against every code change. Focus tests on areas within the application that provide the most coverage but require minimal maintenance. Teams should analyze code from every structural level to look for issues affecting the operational performance of an app. The code should be safe, robust, efficient, and easy to maintain.

                Preventative measures like [SAST](https://docs.gitlab.com/ee/user/application_security/sast/) and [dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) will save time in later phases by reducing the number of code defects before the code is merged and helping developers understand how changes will impact other areas of the application. Establishing test criteria first will also help developers improve the overall quality of their code by providing them with standards to refer to and achieve while writing code.

      - name: topics-copy-block
        data:
          header: 3. Use test results to educate, not punish
          column_size: 10
          blocks:
            - text: |
                Applying test results as negative reinforcement is not a constructive practice. Beyond remediation, results can be leveraged in two ways:
                The individual developer should use results as learnings on how to produce higher quality code.

                At the group level, test results should be scanned for patterns in coding practices that can be improved upon, and used to create standards that will help improve code quality across the entire team or organization.
      - name: topics-copy-block
        data:
          header: "Learn more about DevSecOps:"
          column_size: 10
          blocks:
            - text: |
                [How a security champions program](/blog/2020/10/14/why-security-champions/){data-ga-name="security champions" data-ga-location="body"} can mean better DevSecOps

                How GitLab [enables DevSecOps](/solutions/security-compliance/){data-ga-name="enables devsecops" data-ga-location="body"}

                Understand the [DevSecOps landscape](/developer-survey/){data-ga-name="devsecops landscape" data-ga-location="body"}
