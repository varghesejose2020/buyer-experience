---
  title: What is developer-first security?
  description: Want better code quality and happier developers? Developer-first security puts vulnerability discovery and remediation in an existing IDE. Your security mind blown.
  date_published: 2023-04-18
  date_modified: 2023-04-18
  topics_header:
    data:
      title: What is developer-first security?

      block:
        - metadata:
            id_tag: what-is-developer-first-security
          text: |
            DevSecOps is a software development methodology designed to bring development, security, and operations together on a single unified team. Application security has long been an afterthought in the software development process, and a core vision of DevSecOps is to shift security left — that is, much closer to development — than ever before. Developer-first security is a relatively new concept that could represent the ultimate security shift-left: putting security tools in the hands of a developer so that a large portion of security scanning, testing, and remediation actually happen within a dev’s integrated development environment (IDE).
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevSecOps
      href: /topics/devsecops/
      data-ga-name: devsecops
      data_ga_location: breadcrumb
    - title: What is developer-first security?
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Why application security matters
          href: "#why-application-security-matters"
          data_ga_name: why application security matters
          data_ga_location: side-navigation
        - text: The traditional approach to security
          href: "#the-traditional-approach-to-security"
          data_ga_name: the traditional approach to security
          data_ga_location: side-navigation
        - text: Enter DevSecOps
          href: "#enter-dev-sec-ops"
          data_ga_name: enter devsecops
          data_ga_location: side-navigation
        - text: Developer-first security
          href: "#developer-first-or-in-context-security"
          data_ga_name: developer first security
          data_ga_location: side-navigation
    hyperlinks:
      text: ""
      data: []
    content:
      - name: topics-copy-block
        data:
          header: Why application security matters
          column_size: 10
          blocks:
            - text: |
                A recent Forrester Research survey, Breaches By The Numbers: Adapting To Regional Challenges Is Imperative, April 12, 2022, found that [63% of organizations were breached in the past year](https://www.forrester.com/blogs/breaches-by-the-numbers-adapting-to-regional-challenges-is-imperative/){data-ga-name="forrester security breaches blog post" data-ga-location="body"}, 4% more than the year before. And it’s important to realize [the code is now the primary target](/blog/2020/10/14/why-security-champions/){data-ga-name="security champions blog post" data-ga-location="body"}, rather than the infrastructure. Making things even trickier, some estimates suggest close to [60% of applications are made up of open source code](https://www.helpnetsecurity.com/2018/05/22/open-source-code-security-risk/){data-ga-name="open source code security risk" data-ga-location="body"} — and others put those estimates as high as 80% or 90%. Open source code is inherently more likely to contain vulnerabilities and malicious code than code generated from scratch, but it’s an understandable choice for busy developers trying to deliver quality code under ever-tightening deadlines.
      - name: topics-copy-block
        data:
          header: The traditional approach to security
          column_size: 10
          blocks:
            - text: |
                For years, security was part of a separate organization known to swoop in after the code was committed, find security issues, and demand changes from (perhaps not surprisingly) reluctant developers who’d already moved on to the next project. Security was not just an afterthought; it was a top-down experience delivered by people who were far removed from the challenges of development. It’s not hard to understand why this approach was a major source of frustration for everyone involved.

      - name: topics-copy-block
        data:
          header: Enter DevSecOps
          column_size: 10
          blocks:
            - text: |
                The goal of [DevSecOps](/solutions/security-compliance/){data-ga-name="dev sec ops landing" data-ga-location="body"} was to build on the silo-busting that happened when DevOps was implemented — [now dev, ops, and security all work together](/topics/devsecops/){data-ga-name="devsecops topics landing" data-ga-location="body"}. It’s still early days, but our 2022 [Global DevSecOps Survey](/developer-survey/){data-ga-name="devsecops developer survey" data-ga-location="body"} showed promising signs: almost 29% of security professionals said they’re now part of a cross-functional security team, and 57% of security team members said their organizations have either shifted security left or are planning to this year.

                Friction remains between developers and security, but there are signs that relations are improving. In 2022, fewer security professionals complained about vulnerabilities being identified late in the software development lifecycle or about difficulty getting developers to address security risks.

                From the developer side, over half of developers said they are “fully responsible” for security in their organizations, while another 39% said they feel responsible for security as part of a larger team.
      - name: topics-copy-block
        data:
          header: Developer-first (or in-context) security
          column_size: 10
          blocks:
            - text: |
                To break what feels like a very vicious cycle, experts say it’s time to start thinking about in-context or developer-first security. In a nutshell, developer-first security gives a coder a “developer-friendly” security tool that lives in the IDE and empowers developers to find and fix security issues in a painless manner. Ideally these security controls are automated, allowing a busy developer not to have to think about security requirements to build secure code — the process just happens naturally as part of the coding process.

                Key to the success of developer-first security is a change in perspectives on both sides. Security professionals need to remember developers wear a lot of hats (coding, testing, security, and even some operations functions). Given that, it’s vital that security pros spend time understanding what developers are asked to do — and perhaps learn to code — in order to provide the necessary training, encouragement, and empathy. At the same time developers have to be open to a process change and excited about the opportunity to contribute to code security in a meaningful way.

                Moving security in with the development team, ensuring teams have the right mix of skills, and creating a collegial environment will go a long way toward a successful developer-first security effort.
      - name: topics-cta
        data:
          subtitle: DevSecOps with GitLab
          text: |
            With GitLab, security is built into the CI pipeline, out of the box. Every code commit is automatically scanned for security vulnerabilities in your code and its dependencies.
          column_size: 10
          cta_one:
            text: Learn more
            link: /solutions/security-compliance/
            data_ga_name: dev sec ops solution page
            data_ga_location: body
  components:
    - name: solutions-resource-cards
      data:
        title: Learn more about DevSecOps
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: webcast
              variant: marketing
              alt:
            event_type: "Video"
            header: Citizen-Centric Resiliency In Challenging Times
            link_text: "Watch now"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            href: https://page.gitlab.com/webcast-citizen-centric-resiliency.html
            data_ga_name: Citizen centric resiliency webcast
            data_ga_location: resource cards
          - icon:
              name: article
              variant: marketing
              alt:
            event_type: "Article"
            header: Best Practices for a DoD DevSecOps Culture
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            href: https://page.gitlab.com/resources-article-RightPlatformDoD.html
            data_ga_name: best practices for a DoD devsecops culture
            data_ga_location: resource cards
