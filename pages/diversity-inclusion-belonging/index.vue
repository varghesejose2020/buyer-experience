<script lang="ts">
import Vue from 'vue';
import { getPageMetadata } from '../../common/meta';
import { NextStep } from '../../components/common/index.vue';
import {
  DibHero,
  DibIntroHighlight,
  DibCopyBlock,
  DibWorkforce,
  DibWorkplace,
  DibMarketplace,
  DibConclusion,
} from '../../components/diversity-inclusion-belonging/index.vue';

export default Vue.extend({
  components: {
    DibHero,
    DibIntroHighlight,
    DibCopyBlock,
    DibWorkforce,
    DibWorkplace,
    DibMarketplace,
    DibConclusion,
    NextStep,
  },
  nuxtI18n: false,
  async asyncData({ $content, error }: any) {
    try {
      const data = await $content(
        'diversity-inclusion-belonging/index',
      ).fetch();
      return { data };
    } catch (e) {
      error({ statusCode: 404 });
      return {};
    }
  },
  head() {
    return getPageMetadata((this as any).data, this.$route.path);
  },
});
</script>

<template>
  <div>
    <DibHero :data="data.dib_hero" />
    <DibIntroHighlight :data="data.dib_intro_highlight" />
    <DibCopyBlock :data="data.dib_copy_block_0" />
    <DibCopyBlock :data="data.dib_copy_block_1" />
    <DibWorkforce :data="data.dib_workforce" />
    <DibWorkplace :data="data.dib_workplace" />
    <DibMarketplace :data="data.dib_marketplace" />
    <DibConclusion :data="data.dib_conclusion" />
    <NextStep />
  </div>
</template>
<style scoped lang="scss">
.dedicated-wrapper {
  overflow: hidden;
}

div[role='main'] {
  overflow: clip;
  :deep .markdown-body {
    background: transparent;

    strong {
      font-weight: bold;
    }
  }
}
</style>
